@extends('layouts.app')

@section('content')

    <script>
    $(document).ready(function(){
    $('#crisis-table').DataTable({
        "aaSorting": []    
    });
    $('div.alert').delay(3000).slideUp(300);
    });
    </script>

    <div class="container">
        <h2>Manage Crisis</h2>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
        @if( Auth::user()->hasPrivilege('Activate Crisis') )
        <a class="pull-right" href="crisis/create" style='margin-top: -20px; margin-bottom: 15px'>
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-btn fa-exclamation-circle"></i>Activate Crisis
            </button>
        </a>
        @endif
        <!-- List of Crisis -->
        @if (count($crisis) > 0)
            <table id="crisis-table" class="table table-striped task-table">
                <thead>
                    <th>Crisis</th>
                    <th>Protocol</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($crisis as $c)
                        <tr>
                            <td class="table-text"><div>{{ $c->name }}</div></td>
                            <td class="table-text"><div>@foreach ($c->crisisType() as $cT){{ $cT->crisis_type }} @endforeach Level @foreach ($c->crisisLevel() as $cL){{ $cL->crisis_level }} @endforeach</div></td>
                            <!-- User Update and Delete Button -->
                            <td>
                                <div class="col-md-12">
                                    <div class="btn-group" role="group">
                                        <a href="crisis/{{ $c->id }}">
                                            @if ($c->active)
                                            <button type="submit" id="view-user-{{ $c->id }}" class="btn btn-warning">
                                                <i class="fa fa-btn fa-eye"></i>Manage Crisis
                                            </button>
                                            @else
                                            <button type="submit" id="view-user-{{ $c->id }}" class="btn btn-warning">
                                                <i class="fa fa-btn fa-eye"></i>View Crisis
                                            </button>
                                            @endif
                                        </a>
                                    </div>
                                    @if( Auth::user()->hasPrivilege('Deactivate Crisis') )
                                        @if ($c->active)
                                        <div class="btn-group" role="group">
                                            <form action="crisis/{{ $c->id }}/deactivate" method="POST">
                                                {{ csrf_field() }}
                                                <button type="submit" id="update-user-{{ $c->id }}" class="btn btn-success">
                                                    <i class="fa fa-btn fa-times"></i>Deactivate Crisis
                                                </button>
                                            </form>
                                        </div>
                                        @endif
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
