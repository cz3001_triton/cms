@extends('layouts.app')

@section('content')
    <style>
    th, td {
    padding: 12px;
    text-align: left;
}

    </style>
    <script>
        function showProtocol() {
            var crisisProtocol = document.getElementById('crisisProtocol').value;
            var protocol = document.getElementsByName(crisisProtocol)[0].value;
            document.getElementById('protocol').value = protocol;
        }
    </script>
    
    <div class="container">
        <h2>Update Crisis</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/crisis') }}/{{$crisis->id}}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <table>
                <col width="130">
                <col width="500">
                <tr>
                <div class="form-group{{ $errors->has('crisisProtocol') ? ' has-error' : '' }}">
                    <td style="vertical-align: top;"><label class="control-label">Protocol</label></td>

                    <td>
                        <select type="" class="form-control" name="crisisProtocol" id="crisisProtocol" size="{{count($crisisProtocols)}}" onchange="showProtocol()">
                            @foreach ($crisisProtocols as $crisisProtocol)
                                @if ($crisis->crisis_level_id == $crisisProtocol->crisis_level_id))
                                    <option selected value="{{ $crisisProtocol->crisis_type->id }}|{{ $crisisProtocol->crisis_level->id }}">{{ $crisisProtocol->crisis_type->crisis_type }} Level {{ $crisisProtocol->crisis_level->crisis_level }}</option>
                                @else
                                    <option value="{{ $crisisProtocol->crisis_type->id }}|{{ $crisisProtocol->crisis_level->id }}">{{ $crisisProtocol->crisis_type->crisis_type }} Level {{ $crisisProtocol->crisis_level->crisis_level }}</option>
                                @endif
                                
                            @endforeach
                        </select>

                        @if ($errors->has('crisisProtocol'))
                            <span class="help-block">
                                <strong>{{ $errors->first('crisisProtocol') }}</strong>
                            </span>
                        @endif
                    </td>
                    <td>
                        <textarea disabled name="protocol" id="protocol" class="form-control" style="white-space: pre-line;" rows="8" cols="65"></textarea>
                        @foreach ($crisisProtocols as $crisisProtocol) 
                        <textarea style="white-space: pre-line;" name="{{ $crisisProtocol->crisis_type->id }}|{{ $crisisProtocol->crisis_level->id }}" value="" hidden>{{$crisisProtocol->protocol}}</textarea>
                        @endforeach
                    </td>
                </div>
                </tr>
                <tr>
                <td></td>
                <td>
                <div class="form-group">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-btn fa-edit"></i>Update Crisis
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/crisis/'.$crisis->id) }}">
                            Cancel
                        </a>
                    </div>
                </div>
                </td>
            </form>
            </tr>
            </table>
        </div>
    </div>
@endsection
