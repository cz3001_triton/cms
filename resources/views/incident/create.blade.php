@extends('layouts.app')

@section('content')
    
    <style>
        .navbar {
            margin-bottom: 1vh;
        }
        #map {
            //width: 140vh;
           height: 90vh;
        }
        #checkboxes { 
            position: absolute; 
            top: 200px; 
            right: 500px;
            font-size: 14px;
            background-color: white;
            border: 1px solid black;
            padding: 5px;
        }
    </style>
    <script>
        $(document).ready(function(){
            $('div.alert').delay(3000).slideUp(300);
        });
    </script>
    @include('map.embed')
    <!--{{$incidents}}<br/>   
    @foreach ($incidents as $incident)  
    {{$incident->name}}<br/>   
    {{$incident->type->incident_type}}<br/>   
      @foreach ($incident->status as $status)
        <div>{{ $status->incident_status_type }}</div><br/>
      @endforeach
    @endforeach-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-9" id="map"></div>

            <div class="col-sm-3">
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                <h3>Add Incident</h3>
                <!-- Display Validation Errors -->
                @include('common.errors')

                <form role="form" method="POST" action="{{ url('/intranet/incident') }}">
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class=" control-label">Name</label>

                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                        <label class="control-label">Mobile</label>

                        <input type="text" class="form-control" name="mobile_number" value="{{ old('mobile_number') }}">

                        @if ($errors->has('mobile_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile_number') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                        <label class="control-label">Postal Code</label>

                        <input type="text" class="form-control" name="postal_code" id="postal_code" value="{{ old('postal_code') }}">

                        @if ($errors->has('postal_code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('postal_code') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group{{ $errors->has('unit_number') ? ' has-error' : '' }}">
                        <label class="control-label">Unit Number</label>

                        <input type="text" class="form-control" name="unit_number" value="{{ old('unit_number') }}">

                        @if ($errors->has('unit_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('unit_number') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('incident_type') ? ' has-error' : '' }}">
                        <label class="control-label">Type</label>

                        <select type="" class="form-control" name="incident_type" size="4">
                            @foreach ($incident_types as $incident_type) 
                                <option value="{{ $incident_type->id }}|{{ $incident_type->incident_type }}">{{ $incident_type->incident_type }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('incident_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('incident_type') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                        <label class="control-label">Note</label>

                        <textarea name="note" class="form-control" rows="4">{{ old('note') }}</textarea>

                        @if ($errors->has('note'))
                            <span class="help-block">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                    @if (count($crisis) > 0)
                        <div class="form-group{{ $errors->has('crisis') ? ' has-error' : '' }}">
                            <label class="control-label">Crisis</label>

                            <select type="" class="form-control" name="crisis" size="1">
                                <option value=""></option>
                                @foreach ($crisis as $c) 
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('crisis'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('crisis') }}</strong>
                                </span>
                            @endif
                        </div>
                    @endif
                    <input type="hidden" name="address" id="address">
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-plus"></i>Submit Incident
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="checkboxes">
        <form>
            <input type="checkbox" value="0" id="dengue" onclick="toggleLayer(0);">Dengue</input><br />
            <input type="checkbox" value="1" id="haze" onclick="toggleLayer(1);">Haze</input><br />
            <input type="checkbox" value="2" id="weather" checked = "checked" onclick="toggleLayer(2);">Weather</input><br />
            <input type="checkbox" value="2" id="incidents" checked = "checked" onclick="toggleLayer(3);">Incidents</input>
        </form>
    </div>
@endsection
