@extends('layouts.app')

@section('content')

    <script>
    $(document).ready(function(){
        $('#incident-table').DataTable({
                    "order": [[ 3, "asc" ],[2 , "asc"]]
                });
        $('div.alert').delay(3000).slideUp(300);
    });
    </script>
    
    <div class="container">
        <h2>Manage Incidents</h2>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
        @if( Auth::user()->hasPrivilege('Create Incidents') )
        <a class="pull-right" href="incident/create" style='margin-top: -20px; margin-bottom: 15px'>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-plus"></i>Add Incident
            </button>
        </a>
        @endif
        <!-- List of Incidents -->
        @if (count($incidents) > 0)
            <table id="incident-table" class="table table-striped task-table">
                <col width="42%">
                <col width="13%">
                <col width="5%">
                <col width="42%">
                <thead>
                    <th>Incident</th>
                    <th>Updated On</th>
                    <th>Status</th>
                    <th>Actions</th>
                </thead>
                <tbody>

                    @foreach ($incidents as $incident)
                        <tr>
                            <td class="table-text"><div>{{ $incident->incident }}</div></td>
                            <!--<td class="table-text"><div>{{ $incident->type->incident_type }}</div></td>-->
                            <td class="table-text">
                                @foreach ($incident->status as $status)
                                <div>{{ date('H:i, F d', strtotime($status->pivot->updated_at)) }}</div>
                                @endforeach
                            </td>
                            <td class="table-text">
                                @foreach ($incident->status as $status)
                                <div>{{ $status->incident_status_type }}</div>
                                @endforeach
                            </td>
                            <!-- User Update and Delete Button -->
                            <td>
                                <div class="col-md-12">
                                    <div class="btn-group" role="group">
                                        <button type="submit" id="view-incident-{{ $incident->id }}" class="btn btn-warning" data-toggle="modal" data-target="#viewIncidentModal-{{ $incident->id }}">
                                            <i class="fa fa-btn fa-eye"></i>View
                                        </button>
                                    </div>
                                    <div id="viewIncidentModal-{{ $incident->id }}" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title">Incident</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <iframe id="aiframe" src="incident/{{ $incident->id }}" scrolling="yes" frameborder="0" width="100%">
                                                        <p>Your browser does not support iframes.</p>
                                                    </iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if( Auth::user()->hasPrivilege('Edit Incidents') )
                                    @foreach ($incident->status as $status)                                         
                                            @if ($status->incident_status_type == 'Ongoing')
                                                <div class="btn-group" role="group">
                                                    <a  href="incident/{{ $incident->id }}/edit">
                                                        <button type="submit" id="update-incident-{{ $incident->id }}" class="btn btn-success">
                                                            <i class="fa fa-btn fa-edit"></i>Update
                                                        </button>
                                                    </a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    @if( Auth::user()->hasPrivilege('Assign Resources') )
                                    <div class="btn-group" role="group">
                                        <a  href="incident/{{ $incident->id }}/assign">
                                            <button type="submit" id="assign-incident-{{$incident->id}}" class="btn btn-success">
                                                <i class="fa fa-btn fa-edit"></i>Assign Resource
                                            </button>
                                        </a>
                                    </div>
                                    @endif
                                    @if( Auth::user()->hasPrivilege('Delete Incidents') )
                                    @foreach ($incident->status as $status)                                         
                                            @if ($status->incident_status_type == 'Ongoing')
                                                <div class="btn-group" role="group">
                                                    <form action="incident/{{ $incident->id }}" method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <button type="submit" id="delete-incident-{{ $incident->id }}" class="btn btn-danger">
                                                            <i class="fa fa-btn fa-trash"></i>Delete
                                                        </button>
                                                    </form>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    @if( Auth::user()->hasPrivilege('Edit Incidents') )
                                        @foreach ($incident->status as $status)                                         
                                            @if ($status->incident_status_type == 'Ongoing')
                                                    <div class="btn-group" role="group">
                                                       <form action="incident/{{ $incident->id }}/close" method="POST">
                                                            {{ csrf_field() }}
                                                            <button type="submit" id="done-incident-{{$incident->id}}" class="btn btn-primary">
                                                                <i class="fa fa-btn fa-edit"></i>Close
                                                            </button>
                                                       </form>                       
                                                    </div>
                                            @endif
                                        @endforeach
                                    @endif

                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    
    <style>
        .modal {
          text-align: center;
          padding: 0!important;
          vertical-align: middle;
          height: 100%;
        }

        .modal:before { 
          content: '';
          display: inline-block;    
          vertical-align: middle;
          margin-right: -4px;
        }

        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }

        .modal-body {
            min-height: 300px;
            min-width: 300px; 
        }
        
        .modal-content {
            min-height: 300px;
        }

        #aiframe {
            min-height: 300px;
            height: 100%;
        }
    </style>
@endsection
