@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Update Incident</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/incident') }}/{{$incident->id}}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                
                <div class="form-group{{ $errors->has('incident') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Incident</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="incident" value="{{ $incident->incident }}"> 

                        @if ($errors->has('incident'))
                            <span class="help-block">
                                <strong>{{ $errors->first('incident') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{ $incident->name }}"> 

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Mobile Number</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="mobile_number" value="{{ $incident->mobile_number }}">

                        @if ($errors->has('mobile_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Postal Code</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="postal_code" value="{{ $incident->postal_code }}">

                        @if ($errors->has('postal_code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('postal_code') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('unit_number') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Unit Number</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="unit_number" value="{{ $incident->unit_number }}">

                        @if ($errors->has('unit_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('unit_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('incident_type') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Type</label>
                    <div class="col-md-6">
                        <select type="" class="form-control" name="incident_type" size="{{sizeof($type)}}">
                         @foreach ($type as $incident_type)
                            @if ($incident_type->id == $incident->incident_type_id)
                                <option selected value="{{ $incident_type->id }}">{{ $incident_type->incident_type }}</option>
                            @else
                                <option value="{{ $incident_type->id }}">{{ $incident_type->incident_type }}</option>
                            @endif
                        @endforeach
                        </select>
                        @if ($errors->has('incident_type[]'))
                            <span class="help-block">
                                <strong>{{ $errors->first('incident_type []') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('crisis') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Crisis</label>
                    <div class="col-md-6">
                        <select type="" class="form-control" name="crisis" size="1">
                            <option value=""></option>
                         @foreach ($crisis as $c)
                            @if ($c->id == $incident->crisis_id)
                                <option selected value="{{ $c->id }}">{{ $c->name }}</option>
                            @else
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endif
                        @endforeach
                        </select>
                        @if ($errors->has('crisis'))
                            <span class="help-block">
                                <strong>{{ $errors->first('crisis') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-edit"></i>Update Incident
                        </button>
                        <div class="btn-group pull-right" role="group">
                            <a href="{{ url('/intranet/incident') }}">
                                Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
