@extends('layouts.app')

@section('content')

  <script>
    function getSelect(thisValue){
        var str = thisValue.options[thisValue.selectedIndex].value;
        var ids = str.split("-");
        document.getElementById('incident_id').value = ids[0];
         document.getElementById('dispatch_agency_id').value = ids[1];
          document.getElementById('resource_id').value = ids[2];
    }
  </script>
    <style>
        th, td {
            padding: 6px;
            text-align: left;
        }

        body,h1,h2,h3,h4,h5,h6 {
            font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 700;
        }

    </style>
    <div class="container">
        <h2>Assign Resource to Incident </h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/incident/updateAssign') }}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}

                <p align="left">
                Incident ID: {{$incident->id}}</br>
                Incident Name: {{ $incident->incident }}
                </p>

                <label class="col-md-4 control-label">Select Resource by Dispatch Agency </label>
                <div class="col-md-6">
                    <select type="" class="form-control" name="dispatch_agencys" size="5"  onChange="getSelect(this)">
                     @foreach ($dispatch_agencies as $dispatch_agency)
                                        @foreach ($resource as $resources)                                
                                            @if ($resources->dispatch_agency_id == $dispatch_agency->id)
                                            <option value="{{$incident->id}}-{{$dispatch_agency->id}}-{{$resources->id}}">{{ $dispatch_agency->agency }} - {{ $resources->resource }} , Available : {{ $resources->quantity }}</option> 
                                            @endif                            
                                        @endforeach                              
                    @endforeach
                    </select>
                </div>
                <br />
                <label class="col-md-4 control-label">Enter Quantity</label>
                <div class="col-md-6"><input type="quantity" class="form-control" name="quantity" value="0"></div>
                
                <input type="hidden" class="form-control" id="incident_id" name = "incident_id" >
                <input type="hidden" class="form-control" id="dispatch_agency_id"  name = "dispatch_agency_id" >
                <input type="hidden" class="form-control" id="resource_id" name="resource_id"  >

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-edit"></i>Confirm
                    </button>                
                    </div>
                    <div class="btn-group pull-right" role="group">
                        <a href="{{ url('/intranet/incident') }}">
                            Cancel
                        </a>
                        </div>                   
                </div>
            </form>
        </div>
    </div>
</html>
@endsection
