@extends('layouts.app')

@section('content')
    <style>
    th, td {
    padding: 12px;
    text-align: left;
}

    </style>
    <div class="container">
        <h2>Add Role</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/role') }}">
                {!! csrf_field() !!}

                <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Role</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="role" value="{{ old('role') }}">

                        @if ($errors->has('role'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('privilege[]') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Privilege</label>

                    <div class="col-md-6">
                        @foreach ($privileges as $privilege)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="privilege[]" value="{{ $privilege->id }}"> 
                                    {{ $privilege->privilege }}
                                </label>
                            </div>
                        @endforeach
                        @if ($errors->has('privilege[]'))
                            <span class="help-block">
                                <strong>{{ $errors->first('privilege[]') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <br />
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-plus"></i>Add Role
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/role') }}">
                            Cancel
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
