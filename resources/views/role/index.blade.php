@extends('layouts.app')

@section('content')

    <script>
    $(document).ready(function(){
    $('#role-table').DataTable();
    $('div.alert').delay(3000).slideUp(300);
    });
    </script>

    <div class="container">
        <h2>Manage Roles</h2>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
        @if( Auth::user()->hasPrivilege('Create Roles') )
        <a class="pull-right" href="role/create" style='margin-top: -20px; margin-bottom: 15px'>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-plus"></i>Add Role
            </button>
        </a>
        @endif
            <!-- List of Roles -->
            @if (count($roles) > 0)
                <table id="role-table" class="table table-striped task-table">
                    <thead>
                        <th>Role</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach ($roles as $role)
                            <tr>
                                <td class="table-text"><div>{{ $role->role }}</div></td>
                                <!-- User Update and Delete Button -->
                                <td>
                                    <div class="col-md-12">
                                        <div class="btn-group" role="group">
                                            <a>
                                                <button type="submit" id="view-role-{{ $role->id }}" class="btn btn-warning" data-toggle="modal" data-target="#viewRoleModal-{{ $role->id }}">
                                                    <i class="fa fa-btn fa-eye"></i>View
                                                </button>
                                                <div id="viewRoleModal-{{ $role->id }}" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="modal-title">Role</h3>
                                                            </div>
                                                            <div class="modal-body">
                                                                <iframe id="aiframe" src="role/{{ $role->id }}" scrolling="yes" frameborder="0" width="100%">
                                                                    <p>Your browser does not support iframes.</p>
                                                                </iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        @if( Auth::user()->hasPrivilege('Edit Roles') )
                                        <div class="btn-group" role="group">
                                            <a href="role/{{ $role->id }}/edit">
                                                <button type="submit" id="update-user-{{ $role->id }}" class="btn btn-success">
                                                    <i class="fa fa-btn fa-edit"></i>Update
                                                </button>
                                            </a>
                                        </div>
                                        @endif
                                        @if( Auth::user()->hasPrivilege('Delete Roles') )
                                        <div class="btn-group" role="group">
                                            <form action="role/{{ $role->id }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                
                                                <button type="submit" id="delete-role-{{ $role->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                        </div>
                                            </form>
                                        </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
    </div>

    <style>
        .modal {
          text-align: center;
          padding: 0!important;
          vertical-align: middle;
          height: 100%;
        }

        .modal:before { 
          content: '';
          display: inline-block;    
          vertical-align: middle;
          margin-right: -4px;
        }

        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }

        .modal-body {
            min-height: 300px;
            min-width: 300px; 
        }
        
        .modal-content {
            min-height: 300px;
        }

        #aiframe {
            min-height: 300px;
            height: 100%;
        }
    </style>
@endsection
