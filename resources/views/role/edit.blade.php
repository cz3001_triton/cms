@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Update Role</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/role') }}/{{$role->id}}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Role</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="role" value="{{ $role->role }}">

                        @if ($errors->has('role'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('privilege[]') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Privilege</label>
                    <div class="col-md-6">
                        @foreach ($role->privileges as $role_privilege) 
                            <?php $role_privilege_id[] = $role_privilege->id; ?>
                        @endforeach
                         @foreach ($privileges as $privilege)
                            <div class="checkbox">
                                <label>
                                    @if (in_array($privilege->id, $role_privilege_id))
                                        <input type="checkbox" name="privilege[]" value="{{ $privilege->id }}" checked> 
                                        {{ $privilege->privilege }}
                                    @else
                                        <input type="checkbox" name="privilege[]" value="{{ $privilege->id }}"> 
                                        {{ $privilege->privilege }}
                                    @endif
                                </label>
                            </div>
                        @endforeach
                        @if ($errors->has('privilege[]'))
                            <span class="help-block">
                                <strong>{{ $errors->first('privilege[]') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-btn fa-edit"></i>Update Role
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/role') }}">
                            Cancel
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
