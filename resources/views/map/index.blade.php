@extends('layouts.app')

@section('content')
<style>
    body {
        overflow:hidden;
    }
    #map {
        height:90vh;
    }
    
    .navbar {
        margin-bottom:7px;
    }

    #checkboxes { 
        position: absolute; 
        top: 100px; 
        right: 10px;
        font-family: 'arial', 'sans-serif'; 
        font-size: 14px;
        background-color: white;
        border: 1px solid black;
        padding: 5px;
    }
</style>

@include('map.embed')

<div class="container-fluid" id="map"></div>
<div id="checkboxes">
    <form>
        <input type="checkbox" value="0" id="dengue" onclick="toggleLayer(0);">Dengue</input><br />
        <input type="checkbox" value="1" id="haze" onclick="toggleLayer(1);">Haze</input><br />
        <input type="checkbox" value="2" id="weather" checked = "checked" onclick="toggleLayer(2);">Weather</input><br />
        <input type="checkbox" value="2" id="incidents" checked = "checked" onclick="toggleLayer(3);">Incidents</input>             
    </form>
</div>

@endsection