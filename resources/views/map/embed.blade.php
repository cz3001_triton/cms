
<script type="text/javascript">

    var map;
    
    var layers = [];

    layers[1] = []; //layer haze 
    layers[2] = []; //layer weather
    layers[3] = []; //layer incidents


    var requestWeather;
    var requestHaze;
    var incidents = [];

    var marker;

      /*
    var geoJSON;
    var gettingData = false;
    var openWeatherMapKey = "ABC..."
    var infowindow;*/


    function initMap() {
        //if(document.getElementById('map')) {
        var geocoder = new google.maps.Geocoder;
        map = new google.maps.Map(document.getElementById('map'), {
               center: {lat: 1.3597879867674116 , lng:103.84201748854404 },
               zoom: 12
        });

        //add dengue cluster
        infowindow = new google.maps.InfoWindow();

        initDengue();//initialize dengue

        initWeather();//initialize weather map

        initHaze();//initialize haze map

        initIncidents();

        //for geocoding of postal code

        var postalCodeInputBox = document.getElementById('postal_code'); 

        if(postalCodeInputBox != null) {
          postalCodeInputBox.addEventListener('change', function() {
            geocodeAddress(geocoder, map);
          });
        }
        
        marker = null;
        
        google.maps.event.addListener(map, 'click', function (event) {
            infowindow.close();

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng":event.latLng
            }, function (results, status) {
                console.log(results, status);
                if (status === google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    map.setCenter(results[0].geometry.location);
                    map.setZoom(16);
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        placeName = results[0].formatted_address,
                        latlng = new google.maps.LatLng(lat, lng);

                    console.log(lat + " " + lng + " " + placeName + " " + latlng);
                    
                    if (marker) {
                        moveMarker(placeName, latlng);
                    } else {
                        console.log("oww man");
                        marker = new google.maps.Marker({
                            position: latlng,
                            map: map
                        });
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                        
                        google.maps.event.addListener(marker, "click", function() {
                          infowindow.open(map, marker);
                        });
                    }
                    $.each(results[0].address_components, function (i) {
                        console.log('address_component:'+i);

                        if (results[0].address_components[i].types[0] === "postal_code"){
                            document.getElementById('postal_code').value = results[0].address_components[i].long_name;
                            document.getElementById('address').value = results[0].formatted_address;
                        }
                    });
                    
                }
            });
         });
         
    }

    function initDengue() {
      layers[0] = new google.maps.KmlLayer('https://data.gov.sg/dataset/e7536645-6126-4358-b959-a02b22c6c473/resource/c1d04c0e-3926-40bc-8e97-2dfbb1c51c3a/download/denguecluster.kml',
            {preserveViewport: false, suppressInfoWindows: false});

      layers[0].setMap(null);
    }

    function initWeather() {
        requestWeather = new XMLHttpRequest();
        requestWeather.onload = weatherXmlRespond;
        requestWeather.open("get", 'http://www.nea.gov.sg/api/WebAPI/?dataset=nowcast&keyref=781CF461BB6606AD4AF8F309C0CCE9945B7600AED0A67507', true);
        requestWeather.send();
    }

    function initHaze() {
        layers[1] = [];
        requestHaze = new XMLHttpRequest();
        requestHaze.onload = hazeXmlRespond;
        requestHaze.open("get", "http://www.nea.gov.sg/api/WebAPI/?dataset=psi_update&keyref=781CF461BB6606AD4AF8F309C0CCE9945B7600AED0A67507", true);
        requestHaze.send();
    }

    function initIncidents() {

        <?php
          foreach ($incidents as $incident) {
            echo "incidents.push(" . json_encode($incident) . ");";
          }
        ?>

        console.log(incidents[0]);

        layers[3] = [];

        for(var i = 0; i < incidents.length; i++) {
          var incident = incidents[i];

          var incidentType = incident.type.incident_type;

          var id = incident.incident_type_id;


          var incidentDetail = incident.incident;

          var postalCode = incident.postal_code;

          var iconAdd = null;

          if(id == "1") {
            iconAdd = "/cms/public/assets/ambulance.png";
          } else if(id == "2") {
            iconAdd = "/cms/public/assets/rescue.png";
          } else if(id == "3") {
            iconAdd = "/cms/public/assets/fire.svg";
          } else if(id == "4") {
            iconAdd = "/cms/public/assets/leak.png";
          }

          var htmlString = "<div class='text-center'>" 
              + "<strong>" + incidentType + "</strong>"
              + "<br />" + incidentDetail
              + "</div>";

          var image = {
            url: iconAdd,
            scaledSize: new google.maps.Size(30, 30)
          };

          createIncidentMarker(htmlString, postalCode, image);
        }
    }

    function createIncidentMarker(html, postalCode, image) {
      var geocoder = new google.maps.Geocoder();

      geocoder.geocode({
        address: postalCode,
        country: 'SG'
      }, function(results, status) {
            if(status == google.maps.GeocoderStatus.OK && results.length > 0) {
                layers[3].push(createMarker(results[0].geometry.location, html, image));
                setMapOnAll(3, map);
            }
      });
    }


    var weatherXmlRespond = function() {
        
        var xmlDoc = requestWeather.responseXML;
        var xmlRows = xmlDoc.getElementsByTagName('area');

        for(var i = 0; i < xmlRows.length; i++) {
            var xmlRow = xmlRows[i];

            var xmlLat = parseFloat(xmlRow.getAttribute("lat"));
            var xmlLon = parseFloat(xmlRow.getAttribute("lon"));

            var point = new google.maps.LatLng(xmlLat, xmlLon);

            var xmlName = xmlRow.getAttribute("name");
            var xmlForecast = xmlRow.getAttribute("forecast");

            var xmlIcon = xmlRow.getAttribute("icon");

            var htmlString = "<div class='text-center'> <img src=" 
                + "http://www.nea.gov.sg/Html/Nea/images/common/weather/50px/"
                + xmlIcon + ".png" + ">"
                + "<br /><strong>" + xmlName + "</strong>"
                + "<br />" + xmlForecast
                + "</div>";
            var iconAdd = "http://www.nea.gov.sg/Html/Nea/images/common/weather/50px/" + xmlIcon + ".png";

            // direction to the right and in the Y direction down.
            var image = {
              url: iconAdd,
              scaledSize: new google.maps.Size(25, 25)
            };

            var marker = createMarker(point, htmlString, image);
            
            layers[2].push(marker); 

        }
        setMapOnAll(2, map);

    }

    var hazeXmlRespond = function() {
        var xmlDoc = requestHaze.responseXML;
        var xmlRows = xmlDoc.getElementsByTagName('region');

        layers[1] = [];

        for(var i = 0; i < xmlRows.length; i++) {
            var xmlRow = xmlRows[i];

            var regionId = xmlRow.childNodes[0].textContent;

            var lat = parseFloat(xmlRow.childNodes[1].textContent);
            var lon = parseFloat(xmlRow.childNodes[2].textContent);

            var time = xmlRow.childNodes[3].getAttribute("timestamp");

            var reading3Hr = xmlRow.childNodes[3].childNodes[1].getAttribute("value");
            var reading24Hr = xmlRow.childNodes[3].childNodes[0].getAttribute("value");

            var point = new google.maps.LatLng(lat, lon);

            if(regionId == 'NRS')
              regionId = "National Reporting Stations";
            else if(regionId == 'rNO')
              regionId = "North Region";
            else if(regionId == 'rSO')
              regionId = "South Region";
            else if(regionId == 'rCE')
              regionId = "Central Region";
            else if(regionId == 'rWE')
              regionId = "West Region";
            else
              regionId = "East Region";

            var htmlString = "<div class='text-center'>"
                + "<strong>" + regionId + "</strong>"
                + "<br /> Last update " 
                + time.substring(0, 4) 
                + "-" + time.substring(4, 6) 
                + "-" + time.substring(6, 8)
                + "-" + time.substring(8, 10)
                + ":" + time.substring(10,12)
                + "<br />3-hr PSI " + reading3Hr
                + "<br />24-hr PSI " + reading24Hr
                + "</div>";

            
            var iconAdd = "/cms/public/assets/haze.png";

            var image = {
              url: iconAdd,
              scaledSize: new google.maps.Size(25, 25)
            };

            var marker = createMarker(point, htmlString, image);

            layers[1].push(marker);

          //  batch.push(marker);
        }    
    }

    function createMarker(point, html, icon) {

        var marker;

        if(icon == null) {
          marker = new google.maps.Marker({
            position: point
          });
        }
        else {
          marker = new google.maps.Marker({
            position: point,
            icon: icon 
          });
        }

        var infowindow = new google.maps.InfoWindow({
          content: html
        });

        google.maps.event.addListener(marker, "click", function() {
          infowindow.open(map, marker);
        });
        
        return marker;
    }

    function showMarkers(index) {
      setMapOnAll(index, map);
    }

    function clearMarkers(index) {
      setMapOnAll(index, null);
    }

    function setMapOnAll(index, map) {
      for(var i = 0; i < layers[index].length; i++) {
        layers[index][i].setMap(map);
      }
    }

    function moveMarker(placeName, latlng) {
         //marker.setIcon(icon);
         marker.setPosition(latlng);
         infowindow.setContent(placeName);
         infowindow.open(map, marker);
    }
    
    function toggleLayer(i) {
      if(i == 0) {
        if (layers[i].getMap() === null) {
            console.log(layers[i]);
            layers[i].setMap(map);
        }
        else
            layers[i].setMap(null);
      }
      else if (i==1 || i==2 || i==3) {
          if (layers[i][0].getMap() == null) {
            setMapOnAll(i, map);
          }
          else {
            setMapOnAll(i, null);
          }
      }
    } 
    
    //for geocoding of postal code
    function geocodeAddress(geocoder, map) {
        geocoder.geocode({
          componentRestrictions: {
            country: 'SG',
            postalCode: document.getElementById('postal_code').value,
          }
        }, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            geocodeLatLng(geocoder, map, infowindow, results[0].geometry.location);
            map.setCenter(results[0].geometry.location);
            map.setZoom(16);
            new google.maps.Marker({
              map: map,
              position: results[0].geometry.location
            });
          } else {
            //window.alert('Geocode was not successful for the following reason: ' +
                //status);
          }
        });
      }
      
      function geocodeLatLng(geocoder, map, infowindow, latlng) {
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              map.setCenter(results[0].geometry.location);
              map.setZoom(16);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              //document.getElementById('address').value = results[0].address_components[0].short_name + ' ' + results[0].address_components[2].short_name;
              document.getElementById('address').value = results[0].formatted_address;
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
            } else {
              //window.alert('No results found');
            }
          } else {
            //window.alert('Geocoder failed due to: ' + status);
          }
        });
      }
      
      

</script>

<script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwnRoknsi4hz6oBHts_RZ7UclrBPn9-JM&callback=initMap">
</script>
