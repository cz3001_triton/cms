@extends('layouts.app')

@section('content')

    <script>
    $(document).ready(function(){
        $('#crisis-protocol-table').DataTable();
        $('div.alert').delay(3000).slideUp(300);
    });
    </script>
    
    <div class="container">
        <h2>Manage Crisis Protocols</h2>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
        @if( Auth::user()->hasPrivilege('Create Crisis Protocols') )
        <a class="pull-right" href="crisis-protocol/create">
            <button type="submit" class="btn btn-primary" style='margin-top: -20px; margin-bottom: 15px'>
                <i class="fa fa-btn fa-plus"></i>Add Crisis Protocol
            </button>
        </a>
        @endif

        <!-- List of Crisis Protocol -->
        @if (count($crisisProtocols) > 0)
            <table id="crisis-protocol-table" class="table table-striped task-table">
                <thead>
                    <th>Crisis Type</th>
                    <th>Crisis Level</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($crisisProtocols as $crisisProtocol)
                        <tr>
                            <td class="table-text"><div>{{ $crisisProtocol->crisis_type->crisis_type }}</div></td>
                            <td class="table-text"><div>{{ $crisisProtocol->crisis_level->crisis_level }}</div></td>
                            <!-- User Update and Delete Button -->
                            <td>
                                <div class="col-md-12">
                                    <div class="btn-group" role="group">
                                        <button type="submit" id="view-protocol-{{ $crisisProtocol->crisis_type_id }}{{ $crisisProtocol->crisis_level_id }}" class="btn btn-warning" data-toggle="modal" data-target="#viewProtocolModal-{{ $crisisProtocol->crisis_type_id }}{{ $crisisProtocol->crisis_level_id }}">
                                            <i class="fa fa-btn fa-eye"></i>View
                                        </button>
                                    </div>
                                    <div id="viewProtocolModal-{{ $crisisProtocol->crisis_type_id }}{{ $crisisProtocol->crisis_level_id }}" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title">Crisis Protocol</h3>
                                                </div>
                                                <div class="modal-body">
                                                <iframe id="aiframe" src="crisis-protocol/{{ $crisisProtocol->crisis_type_id }}/{{ $crisisProtocol->crisis_level_id }}" scrolling="no" frameborder="0" width="100%">
                                                    <p>Your browser does not support iframes.</p>
                                                </iframe>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if( Auth::user()->hasPrivilege('Edit Crisis Protocols') )
                                    <div class="btn-group" role="group">
                                        <a  href="crisis-protocol/{{ $crisisProtocol->crisis_type_id }}/{{ $crisisProtocol->crisis_level_id }}/edit">
                                            <button type="submit" id="update-user-{{ $crisisProtocol->id }}" class="btn btn-success">
                                                <i class="fa fa-btn fa-edit"></i>Update
                                            </button>
                                        </a>
                                    </div>
                                    @endif
                                    @if( Auth::user()->hasPrivilege('Delete Crisis Protocols') )
                                    <div class="btn-group" role="group">
                                        <form action="crisis-protocol/{{ $crisisProtocol->crisis_type_id }}/{{ $crisisProtocol->crisis_level_id }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-user-{{ $crisisProtocol->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </div>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
