@extends('layouts.app')

@section('content')
    <style>
    th, td {
    padding: 12px;
    text-align: left;
}

    </style>
    <div class="container">
        <h2>Update Crisis Protocol</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/crisis-protocol') }}/{{ $crisisProtocol->crisis_type_id }}/{{ $crisisProtocol->crisis_level_id }}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <table>
                <col width="130">
                <col width="500">
                <tr>
                    <td>Crisis Type</td>
                    <td>{{ $crisisProtocol->crisis_type->crisis_type }}</td>
                </tr>
                <tr>
                    <td>Crisis Level</td>
                    <td>{{ $crisisProtocol->crisis_level->crisis_level }}</td>
                </tr>
                <tr>             
                <div class="form-group{{ $errors->has('protocol') ? ' has-error' : '' }}">
                    <td style="vertical-align: top;"><label class="control-label">Protocol</label></td>

                    <td>
                        <textarea name="protocol" rows="10" cols="65">{{ $crisisProtocol->protocol }}</textarea>

                        @if ($errors->has('protocol'))
                            <span class="help-block">
                                <strong>{{ $errors->first('protocol') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <td></td>
                <td>
                <div class="form-group">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-btn fa-edit"></i>Update Crisis Protocol
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/crisis-protocol') }}">
                            Cancel
                        </a>
                    </div>
                </div>
                </td>
            </form>
            </tr>
            </table>
        </div>
    </div>
@endsection
