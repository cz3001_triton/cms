@extends('layouts.app')

@section('content')
<style>
th, td {
padding: 12px;
text-align: left;
}

</style>

    <div class="container">
        <h2>Add Crisis Protocol</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/crisis-protocol') }}">
                {!! csrf_field() !!}
                <table>
                <col width="130">
                <col width="500">
                <tr>
                <div class="form-group{{ $errors->has('crisis_type_id') ? ' has-error' : '' }}">
                    <td><label class="control-label">Crisis Type</label></td>

                    <td>
                        <select onchange="getLevel(this);" type="" class="form-control" id="crisis_type" name="crisis_type_id" size="{{count($crisisTypes)}}">
                            @foreach ($crisisTypes as $crisisType) 
                                <option value="{{ $crisisType->id }}">{{ $crisisType->crisis_type }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('crisis_type_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('crisis_type_id') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <div class="form-group{{ $errors->has('crisis_level_id') ? ' has-error' : '' }}">
                    <td><label class="control-label">Crisis Level</label></td>

                    <td>
                        <select type="" class="form-control" name="crisis_level_id" id="crisis_level" size="{{count($crisisLevels)}}">
                            @foreach ($crisisLevels as $crisisLevel) 
                                <option value="{{ $crisisLevel->id }}">{{ $crisisLevel->crisis_level }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('crisis_level_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('crisis_level_id') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <div class="form-group{{ $errors->has('protocol') ? ' has-error' : '' }}">
                    <td style="vertical-align: top;"><label class="control-label">Protocol</label></td>

                    <td>
                        <textarea name="protocol" class="form-control" rows="10" cols="65">{{ old('protocol') }}</textarea>

                        @if ($errors->has('protocol'))
                            <span class="help-block">
                                <strong>{{ $errors->first('protocol') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <td></td>
                <td>
                <div class="form-group" style="padding-left: 15px">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-plus"></i>Add Crisis Protocol
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/crisis-protocol') }}">
                            Cancel
                        </a>
                    </div>
                </div>
                </div>
                </td>
            </form>
            </tr>
            </table>
        </div>
    </div>
@endsection
