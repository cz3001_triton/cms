@extends('layouts.app')

@section('content')
    


    <script>
    $(document).ready(function(){
    $('#user-table').DataTable();
    $('div.alert').delay(3000).slideUp(300);
    });
    </script>

    <div class="container">
        <h2>Manage Users</h2>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
        @if( Auth::user()->hasPrivilege('Create Users') )
        <a class="pull-right" href="user/create" style='margin-top: -20px; margin-bottom: 15px'>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-plus"></i>Add User
            </button>
        </a>
        @endif
            <!-- List of Users -->
            @if (count($users) > 0)
                <table id="user-table" class="table-striped">
                    <col width="20%">
                    <col width="30%">
                    <col width="20%">
                    <col width="30%">
                    <thead>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Email Address</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td class="table-text"><div>{{ $user->name }}</div></td>
                                <td class="table-text">
                                    <div> <!-- Build list of user roles -->
                                        <?php $string = array(); ?> 
                                        @foreach ($user->roles as $roles) 
                                            <?php $string[] = $roles->role; ?>  
                                        @endforeach
                                        {{ implode(" , ",$string) }}
                                    </div>
                                </td>
                                <td class="table-text"><div>{{ $user->email }}</div></td>
                                <!-- User Update and Delete Button -->
                                <td>
                                    <div class="col-md-12">
                                        <div class="btn-group" role="group">
                                            <button type="submit" id="view-user-{{ $user->id }}" class="btn btn-warning" data-toggle="modal" data-target="#viewUserModal-{{ $user->id }}">
                                                <i class="fa fa-btn fa-eye"></i>View
                                            </button>
                                        </div>
                                        <div id="viewUserModal-{{ $user->id }}" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h3 class="modal-title">User</h3>
                                                        </div>
                                                        <div class="modal-body">
                                                        <iframe id="aiframe" src="user/{{ $user->id }}" scrolling="no" frameborder="0" width="100%">
                                                            <p>Your browser does not support iframes.</p>
                                                        </iframe>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @if( Auth::user()->hasPrivilege('Edit Users') )
                                        <div class="btn-group" role="group">
                                            <a href="user/{{ $user->id }}/edit">
                                                <button type="submit" id="update-user-{{ $user->id }}" class="btn btn-success">
                                                    <i class="fa fa-btn fa-edit"></i>Update
                                                </button>
                                            </a>
                                        </div>
                                        @endif
                                        @if( Auth::user()->hasPrivilege('Delete Users') )
                                        <div class="btn-group" role="group">
                                            <form action="user/{{ $user->id }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-user-{{ $user->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        
    </div>

    <style>
        .modal {
          text-align: center;
          padding: 0!important;
          vertical-align: middle;
          height: 100%;
        }

        .modal:before { 
          content: '';
          display: inline-block;    
          vertical-align: middle;
          margin-right: -4px;
        }

        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }

        .modal-body {
            min-height: 20px;
            min-width: 300px; 
        }
        
        .modal-content {
            min-height: 200px;
        }

        #aiframe {
            min-height: 200px;
            height: 100%;
        }
    </style>
@endsection
