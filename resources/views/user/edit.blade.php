@extends('layouts.app')

@section('content')
    <style>
    th, td {
    padding: 12px;
    text-align: left;
}

    </style>
    <div class="container">
        <h2>Update User</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/user') }}/{{$user->id}}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <table>
                <col width="130">
                <col width="500">
                <tr>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <td><label class="control-label">Name</label></td>

                    <td>
                        <input type="text" class="form-control" name="name" value="{{ $user->name }}">

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <td><label class="control-label">E-Mail Address</label></td>

                    <td>
                        <input type="email" class="form-control" name="email" value="{{ $user->email }}">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>                
                <div class="form-group{{ $errors->has('role[]') ? ' has-error' : '' }}">
                    <td><label class="control-label">Role</label></td>
                    <td>
                        @foreach ($user->roles as $user_role) 
                            <?php $user_role_id[] = $user_role->id; ?>
                        @endforeach
                        <select multiple type="" class="form-control" name="role[]" size="{{sizeof($roles)}}">
                         @foreach ($roles as $role)
                            @if (in_array($role->id, $user_role_id))
                                <option selected value="{{ $role->id }}">{{ $role->role }}</option>
                            @else
                                <option value="{{ $role->id }}">{{ $role->role }}</option>
                            @endif
                        @endforeach
                        </select>
                        @if ($errors->has('role[]'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role[]') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <td></td>
                <td>
                <div class="form-group">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-btn fa-edit"></i>Update User
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/user') }}">
                            Cancel
                        </a>
                    </div>
                </div>
                </td>
            </form>
            </tr>
            </table>
        </div>
    </div>
@endsection
