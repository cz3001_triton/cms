<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <style>
        th, td {
            padding: 6px;
            text-align: left;
        }

        body,h1,h2,h3,h4,h5,h6 {
            font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 700;
        }

    </style>
</head>
<body>
    <div class="container">
        <table>
        <col width="130">
        <col width="500">
        <tr>
        <td>Name</td>
        <td> {{ $user->name }} </td>
        </tr>
        <tr>
        <td>Email Address</td>
        <td> {{ $user->email }} </td>
        </tr>
        <tr>
        <td valign="top">Role(s)</td>
        <td> @foreach ($user->roles as $role) 
                            {{ $role->role }}<br />
                        @endforeach </td>
        </tr>
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        <tr>
            <td></td>
        <td></td>
        </tr>
        </table>
    </div>
</body>
</html>