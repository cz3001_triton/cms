@extends('layouts.app')

@section('content')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <div class="container">
        <h2>Update Advisory</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')
            
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/notification') }}/{{$advisory->id}}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Message</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="message" value="{{ $advisory->message }}">
                        
                        @if ($errors->has('message'))
                            <span class="help-block">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('regions[]') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Region</label>
                    <div class="col-md-6">
                        @foreach ($advisory->regions as $regionExisted) 
                            <?php $regionsExist[] = $regionExisted->id; ?>
                        @endforeach
                         @foreach ($regions as $region)
                            <div class="checkbox">
                                <label>
                                    @if (in_array($region->id, $regionsExist))
                                        <input type="checkbox" name="regions[]" value="{{ $region->id }}" checked> 
                                        {{ $region->region }}
                                    @else
                                        <input type="checkbox" name="regions[]" value="{{ $region->id }}"> 
                                        {{ $region->region }}
                                    @endif
                                </label>
                            </div>
                        @endforeach
                        @if ($errors->has('regions[]'))
                            <span class="help-block">
                                <strong>{{ $errors->first('regions[]') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('published') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Published</label>

                    <div class="col-md-6">
                        @if ($advisory->published == 1)
                            <input type="checkbox" data-toggle="toggle" name="published" value="published" checked>
                        @else
                            <input type="checkbox"data-toggle="toggle" name="published" value="published">
                        @endif
                        
                        @if ($errors->has('message'))
                            <span class="help-block">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-btn fa-edit"></i>Update Advisory
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/notification') }}">
                            Cancel
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
