<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<style>
    th, td {
    padding: 6px;
    text-align: left;
}

    </style>
</head>
<body>
    <div class="container">
        <table>
            <col width="150">
            <col width="300">
            <tr>
                <td>Message</td>
                <td>{{ $advisory->message }}</td>
                </td>
            </tr>
            <tr>
                <td>Published</td>
                <td>@if ($advisory->published) Published @else Not Published @endif</td>
                </td>
            </tr>
            <tr>
                <td>Created At</td>
                <td>{{ date('F d, Y H:m:s', strtotime($advisory->created_at)) }}</td>
                </td>
            </tr>
            <tr>
                <td>Updated At</td>
                <td>{{ date('F d, Y H:m:s', strtotime($advisory->updated_at)) }}</td>
                </td>
            </tr>
            <tr>
                <td>Created by</td>
                <td>{{ $advisory->user_created->name }}</td>
                </td>
            </tr>
            <tr>
                <td>Targetted Region</td>
                <!--{{$advisory->regions}}-->
                <td>@foreach ($advisory->regions as $region)
                    {{ $region->region }} <br />
                    @endforeach</td>
                </td>
            </tr>
            <tr><td></td><td></td></tr>
            <tr><td></td><td></td></tr>
            <tr>
                <td><br /></td>
            <td></td>
            </tr>
        </table>
    </div>
    </body>
    </html>