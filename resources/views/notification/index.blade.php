@extends('layouts.app')

@section('content')
<style>
    .modal {
      text-align: center;
      padding: 0!important;
      vertical-align: middle;
    }

    .modal:before {	
      content: '';
      display: inline-block;	
      vertical-align: middle;
      margin-right: -4px;
    }

    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }

    .modal-dialog,
.modal-content {
    /* 80% of window height */
    height: 80%;
}

.modal-body {
    /* 100% = dialog height, 120px = header + footer */
    max-height: calc(100% - 120px);
}

    th {
        text-align:center; 
    }
</style>

<script>
$(document).ready(function(){
    $('#advisory-table').DataTable();
    $('div.alert').delay(3000).slideUp(300);
});
</script>

<div class="container-fluid">
    @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
    <!--<h2>&nbsp&nbsp&nbsp&nbsp&nbsp Update Social Media News Feed</h2>-->

        <div class="col-sm-4" style="margin-top: 30px">
            <div id="live-feed">
<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/Triton_CMS" data-widget-id="708554082374385664" height="400">Tweets by @Triton_CMS</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            @if( Auth::user()->hasPrivilege('Create Advisories') )
            <form role="form" method="POST" action="{{ url('/intranet/notification') }}">
                {!! csrf_field() !!}
                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">

                    <label class="control-label">Message</label>

                    <textarea class="form-control" name="message" rows="5" cols="100" value="{{ old('message') }}"></textarea>
                    @if ($errors->has('message'))
                        <span class="help-block">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group{{ $errors->has('post_to[]') ? ' has-error' : '' }} pull-left">

                        <label class="checkbox-inline"><input type="checkbox" name="post_to[]" value="social">Facebook & Twitter</label>
                        <label class="checkbox-inline"><input type="checkbox" name="post_to[]" value="advisory" data-toggle="modal" data-target="#selectRegionModal">Advisory</label>
                        <label class="checkbox-inline"><input type="checkbox" name="post_to[]" value="sms">SMS</label>
                        
                        @if ($errors->has('post_to[]'))
                        <span class="help-block">
                            <strong>{{ $errors->first('post_to[]') }}</strong>
                        </span>
                        @endif
                </div>

                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-plus"></i>Post
                    </button>
                </div>

                <div id="selectRegionModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title">Region</h3>
                            </div>
                            <div class="modal-body">
                                <div class="form-group{{ $errors->has('region[]') ? ' has-error' : '' }}">

                                        <select multiple type="" class="form-control" name="region[]" size="{{sizeof($regions)}}">
                                            @foreach ($regions as $region) 
                                                <option value="{{ $region->id }}">{{ $region->region }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('region[]'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('region[]') }}</strong>
                                            </span>
                                        @endif
                                        <br />
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-success" data-dismiss="modal" aria-hidden="true">
                                                <i class="fa fa-btn fa-check"></i>Done
                                            </button>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @endif
        </div>
        <div  class="col-sm-8">
            @if( Auth::user()->hasPrivilege('Create Advisories') )
            <h2>Manage Advisories</h2>
            @else
            <h2>Advisories</h2>
            @endif
                @if (count($advisories) > 0)
                <!-- List of Incidents -->
                    <div class="table-responsive">
                        <table id="advisory-table" class="table table-striped task-table">
                            <col width="40%">
                            <col width="25%">
                            <col width="35%">
                            <thead>
                                <th>Advisory</th>
                                <th>Published Date</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach ($advisories as $advisory)
                                <tr>
                                    <td class="table-text"><div style="text-align:justify;">{{ substr($advisory->message,0, 50) }}</div></td>
                                    <td class="table-text"><div style="padding-left:25px;">{{ date('F d, Y', strtotime($advisory->updated_at)) }}</div></td>
                                    <!-- User Update and Delete Button -->
                                    <td>
                                            <div class="btn-group" role="group">
                                                    <button type="submit" id="view-advisory-{{ $advisory->id }}" data-toggle="modal" data-target="#viewNotifModal-{{ $advisory->id }}" class="btn btn-warning">
                                                        <i class="fa fa-btn fa-eye"></i>View
                                                    </button>
                                                    <div id="viewNotifModal-{{ $advisory->id }}" class="modal fade">
                                                    <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h3 class="modal-title">Advisory</h3>
                                                        </div>
                                                        <div class="modal-body">
                                                        <iframe id="aiframe" src="notification/{{ $advisory->id }}" frameborder="0" width="100%" height=350px>
                                                            <p>Your browser does not support iframes.</p>
                                                        </iframe>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                            @if( Auth::user()->hasPrivilege('Edit Advisories') )
                                            <div class="btn-group" role="group">
                                                <a  href="notification/{{ $advisory->id }}/edit">
                                                    <button type="submit" id="update-advisory-{{ $advisory->id }}" class="btn btn-success">
                                                        <i class="fa fa-btn fa-edit"></i>Update
                                                    </button>
                                                </a>
                                            </div>
                                            @endif
                                            @if( Auth::user()->hasPrivilege('Delete Advisories') )
                                            <div class="btn-group" role="group">
                                                <form action="notification/{{ $advisory->id }}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <button type="submit" id="delete-advisory-{{ $advisory->id }}" class="btn btn-danger">
                                                        <i class="fa fa-btn fa-trash"></i>Delete
                                                    </button>
                                                </form>
                                            </div>
                                            @endif                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
        </div>
</div>
@endsection