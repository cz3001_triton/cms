@extends('layouts.app')

@section('content')
    <style>
        .navbar {
            margin-bottom: 0;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <h3>Add Advisory</h3>
                <!-- Display Validation Errors -->
                @include('common.errors')

                <form role="form" method="POST" action="{{ url('/intranet/notification') }}">
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                        <label class=" control-label">Message</label>

                        <input type="text" class="form-control" name="message" value="{{ old('message') }}">

                        @if ($errors->has('message'))
                            <span class="help-block">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div> 
                    <div class="form-group{{ $errors->has('regions[]') ? ' has-error' : '' }}">
                        <td><label class="control-label">Region</label></td>

                        <td>
                            <select multiple type="" class="form-control" name="regions[]" size="5">
                                @foreach ($regions as $region) 
                                    <option value="{{ $region->id }}">{{ $region->region }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('regions[]'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('regions[]') }}</strong>
                                </span>
                            @endif
                        </td>
                    </div>
              

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i>Submit Advisory
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    


@endsection
