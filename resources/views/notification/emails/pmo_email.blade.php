<html>
    <head>
        <style>
            th, td {
                padding: 6px;
                text-align: left;
            }

            body,h1,h2,h3,h4,h5,h6 {
                font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
                font-weight: 700;
            }

        </style>
    </head>
    <body>
        <table>
            <tr>
                <td colspan='3'><h2>Crisis Update</h2></td>
            </tr>
            <tr>
                <td><b>Ongoing Crisis:</b></td>
                <td>@foreach ($crisis as $c) 
                        {{$c->name}}<br/>
                    @endforeach
                </td>
                <td>
                    @foreach ($crisis as $c) 
                        {{$c->crisis_level_id}}<br/>
                    @endforeach
                </td>
            </tr>
            <tr>
                <td><b>Number of incidents:</b></td>
                <td colspan='2'>{{ count($incidents) }}</td>
            </tr>
            <tr>
                <td valign='top'><b>Incident Locations:</b></td>
                <td colspan='2'>@foreach ($incidents as $incident) 
                        {{$incident->incident}}<br/>
                    @endforeach
                </td>
            </tr>
            <tr>
                <td valign='top'><b>Agencies Activated:</b></td>
                <td colspan='2'>@foreach ($agencies as $agency) 
                        {{$agency->agency}}<br/>
                    @endforeach
                </td>
            </tr>
            <tr>
                <td valign='top'><b>State of Resources:</b></td>
                <td>@foreach ($resources as $resource) 
                        {{$resource->resource}}<br/>
                    @endforeach
                </td>
                <td>
                    @foreach ($resources as $resource) 
                        {{ $resource->quantity -  $resource->resource_used}}/{{$resource->quantity}}<br/>
                    @endforeach
                </td>
            </tr>
        </table>
    </body>
</html>