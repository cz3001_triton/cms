<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('assets/favicon.ico') }}">


    <title>Crisis Management System</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}


    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script> 
    
    <style>
          body {
            font-family: 'Lato';
            background-color: #EEE;
        }

        .fa-btn {
            margin-right: 6px;
        }
        
        .btn {
            margin-right: 6px;
        }

        .table-striped>tbody>tr:nth-child(odd)>td, 
        .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #EEE;
        }

        th {
            font-size: 20px; 
        }

        .navbar-font, user {
            color: #fee !important;

        }

        .navbar-font:hover {
            color: #083158 !important;
            background-color: #105390 !important;
        }
        
        .user:hover {
            background-color: #083158 !important;
        }

        .navbar-sides {
            color: #fff !important;
        }

        .user:focus {
            background-color: #083158 !important;
        }

        body,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 700;
        }

        .divide-nav{
          height: 30px;
          background-color: #1e1933;
        }

        .views {
          margin-top: -50px;
        }

        .navbar-nav > li > a, .navbar-brand {
    padding-top:5px !important; 
    height: 35px;
}
        .navbar {
            min-height:35px !important;
        }
//.navbar {min-height:35px !important; border-radius:0px;}
.navbar-collapse {
    //background-color:#1e1933;
}
        .crisis {
            display: inline-block;
            border-radius:2px;
            vertical-align: middle;
            padding: 2px;
            margin-right:3px;
            margin-top: 3px;
        }
        .crisis-1 {
            background-color: #c2e333;
        }
        .crisis-2 {
            background-color: #fffd00;
        }
        .crisis-3 {
            background-color: #ff9900;
        }
        .crisis-4 {
            background-color: #ff6200;
        }
        .crisis-5 {
            background-color: #ff1402;
        }
    </style>
</head>


<body id="app-layout">
    <div class="divide-nav">
    <div class="container">
        <a class="navbar-brand" href="http://localhost/cms/public/intranet">Triton CMS</a>
        
        @if (count($crisis_nav) > 0)
            @foreach ($crisis_nav as $c)
                <div id="crisis" class="crisis crisis-{{$c->crisis_level_id}} pull-right">@foreach ($c->crisisType() as $cT){{ $cT->crisis_type }} @endforeach Level @foreach ($c->crisisLevel() as $cL){{ $cL->crisis_level }} @endforeach</div>
            @endforeach
        @endif
        
        <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav"> 
            <!-- Authentication Links -->
            @if (Auth::guest())
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle navbar-sidesuser user" data-toggle="dropdown" role="button" aria-expanded="false", style="color: #fee" >
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/intranet/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    </ul>
                </li>
            @endif
        </ul>
      </div>
  </div>
</div>

    <nav class="navbar navbar-default" style="background-color: #105390; border-radius:0px; border-color: #105390;">
        <div class="container-fluid">

            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-left" style="font-size: 15px">
                    <li><a class = "navbar-font" href="{{ url('/intranet') }}">Home</a></li>
                    @if( Auth::user()->hasPrivilege('List Users') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/user') }}">Users</a></li>
                    @endif
                    @if( Auth::user()->hasPrivilege('List Roles') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/role') }}">Roles</a></li>
                    @endif
                    @if( Auth::user()->hasPrivilege('List Crisis Types') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/crisis-type') }}">Crisis Types</a></li>
                    @endif
                    @if( Auth::user()->hasPrivilege('List Dispatch Agencies') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/agency') }}">Dispatch Agencies</a></li>
                    @endif
                    @if( Auth::user()->hasPrivilege('List Resources') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/resource') }}">Resources</a></li>
                    @endif
                    @if( Auth::user()->hasPrivilege('List Incidents') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/incident') }}">Incidents</a></li>
                    @endif
                    @if( Auth::user()->hasPrivilege('List Advisories') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/notification') }}">Notification Dashboard</a></li>
                    @endif
                    @if( Auth::user()->hasPrivilege('List Crisis Protocols') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/crisis-protocol') }}">Crisis Protocols</a></li>
                    @endif
                    @if( Auth::user()->hasPrivilege('Activate Crisis') )
                    <li><a class = "navbar-font" href="{{ url('/intranet/crisis') }}">Crisis</a></li>
                    @endif
                    <li><a class = "navbar-font" href="{{ url('/intranet/map') }}">Map</a></li>
                </ul>
                </div>
        </div> <!-- Container -->
    </nav>

<nav class="navbar">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->

            </div>
  </nav>

<div class="views">

    @yield('content')

</div>
<br />
<br />
</body>
</html>
