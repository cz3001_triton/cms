@extends('layouts.app')

@section('content')
    <style>
    th, td {
    padding: 12px;
    text-align: left;
}

    </style>
    <div class="container">
        <h2>Add Resource</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/resource') }}">
                {!! csrf_field() !!}

                <table>
                <col width="140">
                <col width="300">
                <tr>
                <div class="form-group{{ $errors->has('resource') ? ' has-error' : '' }}">
                    <td><label class="control-label">Resource</label></td>
                    <td>
                        <input type="text" class="form-control" name="resource" value="{{ old('resource') }}">

                        @if ($errors->has('resource'))
                            <span class="help-block">
                                <strong>{{ $errors->first('resource') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                
                <tr>
                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                    <td><label class="control-label">Quantity</label></td>
                    <td>
                        <input type="text" class="form-control" name="quantity" value="{{ old('quantity') }}">

                        @if ($errors->has('quantity'))
                            <span class="help-block">
                                <strong>{{ $errors->first('quantity') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>                    
                </tr>
                
                <tr>
                    <div class="form-group{{ $errors->has('dispatch_agency_id') ? ' has-error' : '' }}">
                        <td><label class="control-label">Dispatch Agency</label></td>
                        <td>
                            <select multiple type="" class="form-control" name="dispatch_agency_id" size="{{sizeof($dispatchAgencies)}}" >
                               @foreach ($dispatchAgencies as $dispatchAgency) 
                                   <option value="{{ $dispatchAgency->id }}">{{$dispatchAgency->agency}}</option>
                               @endforeach
                           </select>                       

                        </td>


                        @if ($errors->has('dispatchAgency[]'))
                            <span class="help-block">
                                <strong>{{ $errors->first('dispatch_agency_id') }}</strong>
                            </span>
                        @endif
                    </div>                    
                    
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="form-group">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-plus"></i>Add Resource
                                </button>
                            </div>
                            <div class="btn-group pull-right" role="group">
                                <a href="{{ url('/intranet/resource') }}">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                </table>
            </form>
        </div>
    </div>
@endsection