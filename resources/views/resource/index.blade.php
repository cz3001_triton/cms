@extends('layouts.app')
@section('content')
    <div class="container">

    <script>
    $(document).ready(function(){
        $('#resource-table').DataTable();
        $('div.alert').delay(3000).slideUp(300);
    });
    </script>

        <h2>Manage Resources</h2>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
        <a class="pull-right" href="resource/create" style='margin-top: -20px; margin-bottom: 15px'>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-plus"></i>Add New Resource Type 
            </button>
        </a>
        
        @if (count($resources) > 0)
            <table id="resource-table" class="table table-striped task-table">
                <thead>
                    <th>Health</th>
                    <th>Resource</th>
                    <th>Dispatch_Agency</th>
                    <th>Total</th>
                    <th>Used</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($resources as $resource)
                        <tr>
                            <td><meter min="0" max= {{ $resource->quantity }}  value= {{ $resource->quantity -  $resource->resource_used}} ></meter></td>
                            <td>{{ $resource->resource }}</td>
                            <td>{{ $resource->agency }}</td>
                            <td>{{ $resource->quantity }}</td>
                            <td>{{ $resource->resource_used }}</td>
                            <td>
                                    <div class="btn-group" role="group">
                                        <a href="resource/{{ $resource->id }}/edit">
                                            <button type="submit" id="update-resource-{{ $resource->id }}" class="btn btn-success">
                                                <i class="fa fa-btn fa-edit"></i>Update
                                            </button>
                                        </a>
                                    </div>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
