<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('assets/favicon.ico') }}">

    <title>Crisis Management System</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>

        body {
            font-family: 'Lato';
            background-color: #EEE;
            padding-top: 70px; 
        }

        .fa-btn {
            margin-right: 6px;
        }
        
        #map {
            width: 900px;
            height: 350px;
        }

        .navbar-font {
            color: #fee !important;
        }

        .navbar-font:hover {
            background-color: #083158 !important;
        }

        .navbar-sides {
            color: #fff !important;
        }

        #checkboxes { 
            position: absolute; 
            top: 300px; 
            right: 10px;
            font-size: 14px;
            background-color: white;
            border: 1px solid black;
            padding: 5px;
        }

        #shelter {
            position: absolute;
            top: 260px;
            right: -7px;
        }

        #advisory {
            height: 250px;
            width: 900px; 
            padding:3px; 
        }

        .content
        {
            height:224px;
            overflow-y: auto;
            background:#fff;
        }

        .table-striped>tbody>tr:nth-child(odd)>td, 
        .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #E1EEF2;
        }

        th {
            font-size: 14px; 
        }

        table#advisory-table 
        {
            border-collapse:separate;
            border-spacing:0 5px;
            text-align: justify;
        }

        #shelter-table td,th
        {
            padding: 10px !important;
            vertical-align: top;
        }

        #contact-table td,th
        {
            padding: 10px !important;
            vertical-align: top;
        }

        .modal {
          text-align: center;
          padding: 0!important;
          vertical-align: middle;
        }

        .modal .modal-body {
        max-height: 450px !important;
        overflow-y: auto;
        }  

        .modal:before { 
          content: '';
          display: inline-block;    
          vertical-align: middle;
          margin-right: -4px;
        }

        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }

        .modal-dialog,
        .modal-content {
        /* 80% of window height */
        width: 70%;
        margin: auto;
        margin-top: 30px;
        max-height: calc(100% - 120px);
        overflow-x: auto;
        }
        
        h3 {
            color: #105390;
        }
        p {
            text-align: justify;
        }



    </style>
</head>

<script type = "text/javascript">
function resize() {
    document.getElementById('map').style.height = "600px";
    document.getElementById('shelter').style.top = "20px";
    document.getElementById('checkboxes').style.top = "60px";
} 
</script>

<body id="app-layout"> 
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation" style="background-color: #105390;">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav navbar-sides" href="#" target="_blank">Crisis Management System</a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" style="font-size: 20px">
                    <li>
                      <a class="navbar-font" href="#contact" style="color:#000" data-toggle="modal" data-target="#contact-modal">Contact Us</a>
                    </li>
                </ul>
            </div>
          <!-- /.navbar-collapse -->
        </div>
    <!-- /.container -->
    </nav>
    
        <div id="contact-modal" class="modal fade">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" style="color: #c51a1a">Contact Us</h3>
                </div>
                <div class="modal-body">
                <table id="contact-table">
                <tr>
                <td>
                <h3>Ministry of Home Affairs</h3>
                <p style="font-size: 18px">New Phoenix Park<br>28 Irrawaddy Road<br>Singapore 329560<br><br>
                <b>Tel:</b> 6478 7010<br><b>Fax:</b> 6254 6250</p>
                </td>
                <td>
                <h3>Singapore Police Force</h3>
                <p style="font-size: 18px">New Phoenix Park<br>28 Irrawaddy Road<br>Singapore 329560<br><br>
                <b>Tel:</b> 6353 0000<br></p>
                </td>
                </tr>
                </table>
               </div>
            </div>
        </div>
    </div>

    <a name="home"></a>
    
    <div class="intro-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3" id="live-feed">
<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/Triton_CMS" data-widget-id="708554082374385664">Tweets by @Triton_CMS</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
                
                <div class="col-md-9">
                    @if (count($advisories) > 0)
                    <div class="container" id="advisory">
                        <div class="content">
                            <table class="table-striped" id="advisory-table" cellspacing="0px">
                            <col width="180">
                            <col width="720">
                            <caption style="text-align:center; font-size: 20px; color: #105390;"><b>Advisory by Singapore Government</b></caption>
                            <thead>
                                <th style="padding-left: 20px">Date</th>
                                <th style="padding-right: 20px">Message</th>
                            </thead>
                            <tbody>
                                @foreach ($advisories as $advisory)
                                <tr>
                                    <td style="padding-left: 20px">{{ date('g:i A, F d', strtotime($advisory->created_at)) }}</td>
                                    <td style="padding-right: 20px">{{ $advisory->message }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                    <div id="map"></div>
                        <div id="shelter">
                        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#shelter-list">
                        <i class="fa fa-btn fa-home"></i>Shelter List
                        </button>
                        <div id="shelter-list" class="modal fade">
                                <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h3 class="modal-title" style="color: #c51a1a">Shelter List</h3>
                                    </div>
                                    <div class="modal-body">
                                    <h3 style="margin-top: -5px;">MRT Shelters</h3>
                                    <p>Public shelters are incorporated in underground MRT stations to provide protection for the population in times of Emergency. There are 40 underground MRT stations which are hardened to double-up as public shelters. They are:</p>
                                    <table id="shelter-table" class="table-bordered" cellspacing="0px">
                                        <thead>
                                            <th>North-South Line</th>
                                            <th>East-West Line</th>
                                            <th>North-East Line</th>
                                            <th>Circle Line</th>
                                            <th>Downtown Line</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td>Braddell<br>
                                            Newton<br>
                                            Somerset<br>
                                            Raffles Place<br>
                                            City Hall<br>
                                            Marina Bay</td>
                                            <td>Lavender<br>
                                            Bugis<br>
                                            Tiong Bahru</td>
                                            <td>Harbourfront<br>
                                            Outram Park<br>
                                            Chinatown<br>
                                            Clarke Quay<br>
                                            Boon Keng<br>
                                            Little India<br>
                                            Farrer Park<br>
                                            Potong Pasir<br>
                                            Woodleigh<br>
                                            Serangoon<br>
                                            Hougang<br>
                                            Kovan<br>
                                            Buangkok</td>
                                            <td>Mountbatten<br>
                                            Dakota<br>
                                            MacPherson<br>
                                            Bartley<br>
                                            Lorong Chuan<br>
                                            Bishan<br>
                                            Botanic Garden<br>
                                            Buona Vista<br>
                                            Labrador<br>
                                            Caldecott<br>
                                            Haw Par Villa</td>
                                            <td>Bukit Panjang<br>
                                            Cashew<br>
                                            Hillview<br>
                                            Beauty World<br>
                                            King Albert Park<br>
                                            Sixth Avenue<br>
                                            Tan Kah Kee</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br><p>The size of the average MRT shelter ranges from 2400 m2 to 3200 m2 and they are able to accommodate about 6,000 to 8,000 shelterees. The larger MRT shelters range from 6,400 m2 to 7,600 m2 and can accommodate 16,000 to 19,000 shelterees. MRT shelters are designed with facilities to ensure the shelter environment is tolerable for all shelterees during shelter occupation. These facilities include protective blast doors, decontamination facilities, ventilation system, power and water supply systems and dry toilet system etc.</p>
                                    <h3>Housing Development Board (HDB) Shelters</h3>
                                    <p>The HDB public shelters are located in the basements or at the void-decks of certain HDB residential apartment blocks.</p>
                                    <h3>Schools</h3>
                                    <p>Basement air-rifle ranges of many new secondary schools are hardened as shelters.</p>
                                    <h3>Community Centres / Clubs</h3>
                                    <p>Public shelters are found in certain community centres / clubs. Shelters here are generally used by the centres / clubs themselves for their own peacetime purpose.</p>
                                    <h3>Other Public Developments</h3>
                                    <p>Other public developments also have public shelters. Examples of their peacetime uses include basement carpark, training activity room and workshop.</p>
                                   </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div id="checkboxes">
                            <form>
                                <input type="checkbox" value="0" id="dengue" onclick="toggleLayer(0);">Dengue</input><br />
                                <input type="checkbox" value="1" id="haze" onclick="toggleLayer(1);">Haze</input><br />
                                <input type="checkbox" value="2" id="weather" checked = "checked" onclick="toggleLayer(2);">Weather</input><br />
                                <input type="checkbox" value="2" id="incidents" checked = "checked" onclick="toggleLayer(3);">Incidents</input>
                            </form>
                        </div>
                    @if (count($advisories) == 0)
                    <script>window.onload = resize();</script>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <p id="demo"></p>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
    <script>
        $(document).ready(function() {
            getLocation();
            
            var x = document.getElementById("demo");
            
            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                    //console.log(navigator.geolocation);
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }
            function showPosition(position) {
                // x.innerHTML = "Latitude: " + position.coords.latitude + 
                // "<br>Longitude: " + position.coords.longitude;
                
                var initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                
                var infoWindow = new google.maps.InfoWindow({map: map});
                
                console.log(infoWindow);

                infoWindow.setPosition(initialLocation);
                infoWindow.setContent('Your Location'); 
                map.setCenter(initialLocation);
            }
        });
    </script>

    @include('map.embed')

</body>



</html>
