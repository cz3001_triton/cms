@extends('layouts.app')

@section('content')

    <script>
    $(document).ready(function(){
    $('#agency-table').DataTable();
    $('div.alert').delay(3000).slideUp(300);
    });
    </script>

    <div class="container">
        <h2>Manage Dispatch Agencies</h2>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
        
        @if( Auth::user()->hasPrivilege('Create Dispatch Agencies') )
        <a class="pull-right" href="agency/create" style='margin-top: -20px; margin-bottom: 15px'>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-plus"></i>Add Dispatch Agency
            </button>
        </a>
        @endif
            <!-- List of Users -->
            @if (count($agencies) > 0)
                <table id="agency-table" class="table table-striped task-table">
                    <thead>
                        <th>Agency</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach ($agencies as $agency)
                            <tr>
                                <td class="table-text"><div>{{ $agency->agency }}</div></td>
                                <!-- User Update and Delete Button -->
                                <td>
                                    <div class="col-md-12">
                                        <div class="btn-group" role="group">
                                            <a>
                                                <button type="submit" id="view-agency-{{ $agency->id }}" class="btn btn-warning" data-toggle="modal" data-target="#viewAgencyModal-{{ $agency->id }}">
                                                    <i class="fa fa-btn fa-eye"></i>View
                                                </button>
                                                <div id="viewAgencyModal-{{ $agency->id }}" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="modal-title">Agency</h3>
                                                            </div>
                                                            <div class="modal-body">
                                                            <iframe id="aiframe" src="agency/{{ $agency->id }}" scrolling="no" frameborder="0" width="100%">
                                                                <p>Your browser does not support iframes.</p>
                                                            </iframe>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        @if( Auth::user()->hasPrivilege('Edit Dispatch Agencies') )
                                        <div class="btn-group" role="group">
                                            <a href="agency/{{ $agency->id }}/edit">
                                                <button type="submit" id="update-user-{{ $agency->id }}" class="btn btn-success">
                                                    <i class="fa fa-btn fa-edit"></i>Update
                                                </button>
                                            </a>
                                        </div>
                                        @endif
                                        @if( Auth::user()->hasPrivilege('Delete Dispatch Agencies') )
                                        <div class="btn-group" role="group">
                                            <form action="agency/{{ $agency->id }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-user-{{ $agency->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
		
    </div>

    <style>
        .modal {
          text-align: center;
          padding: 0!important;
          vertical-align: middle;
          height: 100%;
        }

        .modal:before { 
          content: '';
          display: inline-block;    
          vertical-align: middle;
          margin-right: -4px;
        }

        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }

        .modal-body {
            min-height: 20px;
            min-width: 300px; 
        }
        
        .modal-content {
            min-height: 200px;
        }

        #aiframe {
            min-height: 200px;
            height: 100%;
        }
    </style>
@endsection
