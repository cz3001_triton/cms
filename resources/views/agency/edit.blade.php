@extends('layouts.app')

@section('content')
    <style>
    th, td {
    padding: 12px;
    text-align: left;
}

    </style>
    <div class="container">
        <h2>Update Dispatch Agency</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/agency') }}/{{$agency->id}}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <table>
                <col width="130">
                <col width="500">
                <tr>
                <div class="form-group{{ $errors->has('agency') ? ' has-error' : '' }}">
                    <td><label class="control-label">Agency</label></td>

                    <td>
                        <input type="text" class="form-control" name="agency" value="{{ $agency->agency }}">

                        @if ($errors->has('agency'))
                            <span class="help-block">
                                <strong>{{ $errors->first('agency') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                    <td><label class="control-label">Number</label></td>

                    <td>
                        <input type="text" class="form-control" name="number" value="{{ $agency->number }}">

                        @if ($errors->has('number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>                
                <div class="form-group{{ $errors->has('incidentType[]') ? ' has-error' : '' }}">
                    <td><label class="control-label">Role</label></td>
                    <td>
                        @foreach ($agency->default_assignments as $default_assignment)
                            <?php $default_assignment_id[] = $default_assignment->id; ?>
                        @endforeach
                        <select multiple type="" class="form-control" name="incidentType[]" size="{{sizeof($incidentTypes)+1}}">
                            <option value=""></option>
                         @foreach ($incidentTypes as $incidentType)
                            @if (in_array($incidentType->id, $default_assignment_id))
                                <option selected value="{{ $incidentType->id }}">{{ $incidentType->incident_type }}</option>
                            @else
                                <option value="{{ $incidentType->id }}">{{ $incidentType->incident_type }}</option>
                            @endif
                        @endforeach
                        </select>
                        @if ($errors->has('incidentType[]'))
                            <span class="help-block">
                                <strong>{{ $errors->first('incidentType[]') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <td></td>
                <td>
                <div class="form-group">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-btn fa-edit"></i>Update Dispatch Agency
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/agency') }}">
                            Cancel
                        </a>
                    </div>
                </div>
                </td>
            </form>
            </tr>
            </table>
        </div>
    </div>
@endsection
