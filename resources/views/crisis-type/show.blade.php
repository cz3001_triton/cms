@extends('layouts.app')

@section('content')
<style>
    th, td {
    padding: 6px;
    text-align: left;
}

    </style>
    <div class="container">
        <h2>Crisis Type</h2><br />
        <table>
        <col width="130">
        <col width="500">
        <tr>
        <td>Crisis Type</td>
        <td> {{ $crisisType->crisis_type }} </td>
        </tr>
        <td></td>
        <td>
        <a href="{{ url('/intranet/crisis-type') }}">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-chevron-left"></i>Back
            </button>
        </a></td>
        </tr> 
    </div>
@endsection
