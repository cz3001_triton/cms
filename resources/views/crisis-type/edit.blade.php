@extends('layouts.app')

@section('content')
    <style>
    th, td {
    padding: 12px;
    text-align: left;
}

    </style>
    <div class="container">
        <h2>Update Crisis Type</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/crisis-type') }}/{{$crisisType->id}}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <table>
                <col width="130">
                <col width="500">
                <tr>
                <div class="form-group{{ $errors->has('crisis_type') ? ' has-error' : '' }}">
                    <td><label class="control-label">Name</label></td>

                    <td>
                        <input type="text" class="form-control" name="crisis_type" value="{{ $crisisType->crisis_type }}">

                        @if ($errors->has('crisis_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('crisis_type') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <td></td>
                <td>
                <div class="form-group">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-btn fa-edit"></i>Update Crisis Type
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/crisis-type') }}">
                            Cancel
                        </a>
                    </div>
                </div>
                </td>
            </form>
            </tr>
            </table>
        </div>
    </div>
@endsection
