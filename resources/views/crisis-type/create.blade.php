@extends('layouts.app')

@section('content')
    <style>
    th, td {
    padding: 12px;
    text-align: left;
}

    </style>

    <div class="container">
        <h2>Add Crisis Type</h2>
        <div class="col-sm-12">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/intranet/crisis-type') }}">
                {!! csrf_field() !!}
                <table>
                <col width="130">
                <col width="500">
                <tr>
                <div class="form-group{{ $errors->has('crisis_type') ? ' has-error' : '' }}">

                    <td><label class="control-label">Crisis Type</label></td>

                    <td>
                        <input type="text" class="form-control" name="crisis_type" value="{{ old('crisis_type') }}">
                        @if ($errors->has('crisis_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('crisis_type') }}</strong>
                            </span>
                        @endif
                    </td>
                </div>
                </tr>
                <tr>
                <td></td>
                <td>
                <div class="form-group" style="padding-left: 15px">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-plus"></i>Add Crisis Type
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('/intranet/crisis-type') }}">
                            Cancel
                        </a>
                    </div>
                </div>
                </div>
                </td>
            </form>
            </tr>
            </table>
        </div>
    </div>
@endsection
