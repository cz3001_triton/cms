@extends('layouts.app')

@section('content')

    <script>
    $(document).ready(function(){
        $('#crisis-type-table').DataTable();
        $('div.alert').delay(3000).slideUp(300);
    });
    </script>
    
    <div class="container">
        <h2>Manage Crisis Types</h2>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
            <br/>
        @endif
        
        @if( Auth::user()->hasPrivilege('Create Crisis Types') )
        <a class="pull-right" href="crisis-type/create" style='margin-top: -20px; margin-bottom: 15px'>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-plus"></i>Add Crisis Type
            </button>
        </a>
        @endif
        <!-- List of Crisis Type -->
        @if (count($crisisTypes) > 0)
            <table id="crisis-type-table" class="table table-striped task-table">
                <thead>
                    <th>Crisis Type</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($crisisTypes as $crisisType)
                        <tr>
                            <td class="table-text"><div>{{ $crisisType->crisis_type }}</div></td>
                            <!-- Crisis Type Update and Delete Button -->
                            <td>
                                <div class="col-md-12">
                                    <!--
                                    <div class="btn-group" role="group">
                                        <a href="crisis-type/{{ $crisisType->id }}">
                                            <button type="submit" id="view-crisis-type-{{ $crisisType->id }}" class="btn btn-warning">
                                                <i class="fa fa-btn fa-eye"></i>View
                                            </button>
                                        </a>
                                    </div>-->
                                    @if( Auth::user()->hasPrivilege('Edit Crisis Types') )
                                    <div class="btn-group" role="group">
                                        <a  href="crisis-type/{{ $crisisType->id }}/edit">
                                            <button type="submit" id="update-crisis-type-{{ $crisisType->id }}" class="btn btn-success">
                                                <i class="fa fa-btn fa-edit"></i>Update
                                            </button>
                                        </a>
                                    </div>
                                    @endif
                                    @if( Auth::user()->hasPrivilege('Delete Crisis Types') )
                                    <div class="btn-group" role="group">
                                        <form action="crisis-type/{{ $crisisType->id }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-crisis-type-{{ $crisisType->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </div>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
