<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crisis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('crisis_type_id')->unsigned();
            $table->integer('crisis_level_id')->unsigned();
            $table->boolean('active');
            $table->timestamps();
            
            $table->foreign('crisis_type_id')->references('crisis_type_id')->on('crisis_protocols');
            $table->foreign('crisis_level_id')->references('crisis_level_id')->on('crisis_protocols');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crisis');
    }
}
