<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_status', function (Blueprint $table) {
            $table->integer('incident_id')->unsigned();
            $table->integer('incident_status_type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('incident_id')->references('id')->on('incidents');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('incident_status_type_id')->references('id')->on('incident_status_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incident_status');
    }
}
