<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentDispatchAgencyResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_dispatch_agency_resources', function (Blueprint $table) {
            $table->integer('incident_id')->unsigned();
            $table->integer('dispatch_agency_id')->unsigned();
            $table->integer('resource_id')->unsigned();
            $table->integer('quantity');

            $table->foreign('incident_id')->references('incident_id')->on('incident_dispatch_agencies');
            $table->foreign('dispatch_agency_id')->references('dispatch_agency_id')->on('incident_dispatch_agencies');
            $table->foreign('resource_id')->references('id')->on('resources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incident_dispatch_agency_resources');
    }
}
