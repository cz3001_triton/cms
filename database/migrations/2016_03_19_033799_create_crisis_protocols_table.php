<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrisisProtocolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crisis_protocols', function (Blueprint $table) {
            $table->integer('crisis_type_id')->unsigned();
            $table->integer('crisis_level_id')->unsigned();
            $table->text('protocol');
            $table->timestamps();

            $table->foreign('crisis_type_id')->references('id')->on('crisis_types');
            $table->foreign('crisis_level_id')->references('id')->on('crisis_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crisis_protocols');
    }
}
