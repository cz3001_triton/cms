<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentDispatchAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_dispatch_agencies', function (Blueprint $table) {
            $table->integer('incident_id')->unsigned();
            $table->integer('dispatch_agency_id')->unsigned();

            $table->foreign('incident_id')->references('id')->on('incidents');
            $table->foreign('dispatch_agency_id')->references('id')->on('dispatch_agencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incident_dispatch_agencies');
    }
}
