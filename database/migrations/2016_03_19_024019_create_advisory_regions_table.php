<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvisoryRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advisory_regions', function (Blueprint $table) {
            $table->integer('advisory_id')->unsigned();
            $table->integer('region_id')->unsigned();

            $table->foreign('advisory_id')->references('id')->on('advisories');
            $table->foreign('region_id')->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advisory_regions');
    }
}
