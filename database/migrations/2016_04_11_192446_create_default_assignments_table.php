<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_assignments', function (Blueprint $table) {
            $table->integer('incident_type_id')->unsigned();
            $table->integer('dispatch_agency_id')->unsigned();

            $table->foreign('incident_type_id')->references('id')->on('incident_types');
            $table->foreign('dispatch_agency_id')->references('id')->on('dispatch_agencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('default_assignments');
    }
}
