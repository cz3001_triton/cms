<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('incident');
            $table->string('name');
            $table->string('mobile_number');
            $table->string('postal_code')->nullable();
            $table->string('unit_number')->nullable();
            $table->integer('incident_type_id')->unsigned();
            $table->integer('crisis_id')->unsigned()->nullable();
            $table->text('note')->nullable();
            
            $table->foreign('incident_type_id')->references('id')->on('incident_types');
            $table->foreign('crisis_id')->references('id')->on('crisis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incidents');
    }
}
