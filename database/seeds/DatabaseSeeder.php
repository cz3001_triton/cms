<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Privilege, App\Role, App\User, App\Region, App\Resource, App\DispatchAgency, App\IncidentType, App\IncidentStatusType, App\CrisisType, App\CrisisLevel, App\CrisisProtocol, App\Crisis;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Privileges table */
        Privilege::create(['privilege' => 'List Users']); //1
        Privilege::create(['privilege' => 'Create Users']);
        Privilege::create(['privilege' => 'Edit Users']);
        Privilege::create(['privilege' => 'Delete Users']);
        
        Privilege::create(['privilege' => 'List Roles']); //5
        Privilege::create(['privilege' => 'Create Roles']);
        Privilege::create(['privilege' => 'Edit Roles']);
        Privilege::create(['privilege' => 'Delete Roles']);
        
        Privilege::create(['privilege' => 'List Incident Types']); //9
        Privilege::create(['privilege' => 'Create Incident Types']);
        Privilege::create(['privilege' => 'Edit Incident Types']);
        Privilege::create(['privilege' => 'Delete Incident Types']);
        
        Privilege::create(['privilege' => 'List Incident Status Types']); //13
        Privilege::create(['privilege' => 'Create Incident Status Types']);
        Privilege::create(['privilege' => 'Edit Incident Types']);
        Privilege::create(['privilege' => 'Delete Incident Types']);
        
        Privilege::create(['privilege' => 'List Dispatch Agencies']); //17
        Privilege::create(['privilege' => 'Create Dispatch Agencies']);
        Privilege::create(['privilege' => 'Edit Dispatch Agencies']);
        Privilege::create(['privilege' => 'Delete Dispatch Agencies']);
        Privilege::create(['privilege' => 'Assign Dispatch Agencies']); //21
        
        Privilege::create(['privilege' => 'List Resources']); //22
        Privilege::create(['privilege' => 'Create Resources']);
        Privilege::create(['privilege' => 'Edit Resources']);
        Privilege::create(['privilege' => 'Delete Resources']);
        Privilege::create(['privilege' => 'Assign Resources']); //26
        
        Privilege::create(['privilege' => 'List Crisis Types']); //27
        Privilege::create(['privilege' => 'Create Crisis Types']);
        Privilege::create(['privilege' => 'Edit Crisis Types']);
        Privilege::create(['privilege' => 'Delete Crisis Types']);
        
        Privilege::create(['privilege' => 'List Crisis Levels']); //31
        Privilege::create(['privilege' => 'Create Crisis Levels']);
        Privilege::create(['privilege' => 'Edit Crisis Levels']);
        Privilege::create(['privilege' => 'Delete Crisis Levels']);
        
        Privilege::create(['privilege' => 'List Regions']); //35
        Privilege::create(['privilege' => 'Create Regions']);
        Privilege::create(['privilege' => 'Edit Regions']);
        Privilege::create(['privilege' => 'Delete Regions']);
        
        Privilege::create(['privilege' => 'List Incidents']); //39
        Privilege::create(['privilege' => 'Create Incidents']);
        Privilege::create(['privilege' => 'Edit Incidents']);
        Privilege::create(['privilege' => 'Delete Incidents']);
        
        Privilege::create(['privilege' => 'List Advisories']); //43
        Privilege::create(['privilege' => 'Create Advisories']);
        Privilege::create(['privilege' => 'Edit Advisories']);
        Privilege::create(['privilege' => 'Delete Advisories']);
        
        Privilege::create(['privilege' => 'List Crisis Protocols']); //47
        Privilege::create(['privilege' => 'Create Crisis Protocols']);
        Privilege::create(['privilege' => 'Edit Crisis Protocols']);
        Privilege::create(['privilege' => 'Delete Crisis Protocols']);
        
        Privilege::create(['privilege' => 'Activate Crisis']); //51
        Privilege::create(['privilege' => 'Edit Crisis']);
        Privilege::create(['privilege' => 'Deactivate Crisis']);
        
        /* Roles table */
        $role = Role::create(['role' => 'Administrator',]);
        $role -> save();
        $role->privileges()->sync([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,27,28,29,30,31,32,33,34,35,36,37,38]);
        
        $role = Role::create(['role' => 'Call Center Operator',]);
        $role -> save();
        $role->privileges()->sync([39,40,41,42,43]);
        
        $role = Role::create(['role' => 'Resource Controller',]);
        $role -> save();
        $role->privileges()->sync([17,21,22,23,24,25,26,39,47]);
        
        $role = Role::create(['role' => 'MHA Decision Maker',]);
        $role -> save();
        $role->privileges()->sync([39,43,44,45,46,47,48,49,50,51,52,53]);
        
        $role = Role::create(['role' => 'MHA Public Relations',]);
        $role -> save();
        $role->privileges()->sync([39,43,44,45,46,47]);
        
        $role = Role::create(['role' => 'Government Agency',]);
        $role -> save();
        $role->privileges()->sync([39,40,41,43,47]);
        
        /* Users table */
        $user = User::create([
            'id' => '1', 
            'name' => 'Administrator',
            'email' => 'admin@tritoncms.gov.sg',
            'password' => bcrypt('password'),
        ]);
        $user -> save();
        $user->roles()->sync([1]);
        
        $user = User::create([
            'id' => '2', 
            'name' => 'Call Center Operator',
            'email' => 'cco@tritoncms.gov.sg',
            'password' => bcrypt('password'),
        ]);
        $user -> save();
        $user->roles()->sync([2]);
        
        $user = User::create([
            'id' => '3', 
            'name' => 'Resource Controller',
            'email' => 'rc@tritoncms.gov.sg',
            'password' => bcrypt('password'),
        ]);
        $user -> save();
        $user->roles()->sync([3]);
        
        $user = User::create([
            'id' => '4', 
            'name' => 'MHA Decision Maker',
            'email' => 'mhadm@tritoncms.gov.sg',
            'password' => bcrypt('password'),
        ]);
        $user -> save();
        $user->roles()->sync([4]);
        
        $user = User::create([
            'id' => '5', 
            'name' => 'MHA Public Relations',
            'email' => 'mhapr@tritoncms.gov.sg',
            'password' => bcrypt('password'),
        ]);
        $user -> save();
        $user->roles()->sync([5]);
        
        $user = User::create([
            'id' => '6', 
            'name' => 'Government Agency',
            'email' => 'govtagency@tritoncms.gov.sg',
            'password' => bcrypt('password'),
        ]);
        $user -> save();
        $user->roles()->sync([6]);

        
        /* Incident_Status_Types table */
        IncidentStatusType::create(['incident_status_type' => 'Ongoing']);
        IncidentStatusType::create(['incident_status_type' => 'Closed']);
        
        
        /* Dispatch_Agencies table */
        DispatchAgency::create(['agency' => 'SPF', 'number' => '+6583105586']);
        DispatchAgency::create(['agency' => 'SCDF', 'number' => '+6583105586']);
        DispatchAgency::create(['agency' => 'Singapore Power', 'number' => '+6583105586']);
        
        
        /* IncidentTypes table */
        $incidentType = IncidentType::create(['incident_type' => 'Emergency Ambulance']);
        $incidentType->agencies()->sync([2]);
        
        $incidentType = IncidentType::create(['incident_type' => 'Rescue and Evacuation']);
        $incidentType->agencies()->sync([2]);
        
        $incidentType = IncidentType::create(['incident_type' => 'Fire-Fighting']);
        $incidentType->agencies()->sync([2]);
        
        $incidentType = IncidentType::create(['incident_type' => 'Gas Leak Control']);
        $incidentType->agencies()->sync([3]);
        
        
        /* Resource table */
        Resource::create(['dispatch_agency_id' => 2, 'resource' => 'Ambulance','quantity' => '150']);
        Resource::create(['dispatch_agency_id' => 2, 'resource' => 'Fire Engine','quantity' => '100']);
        Resource::create(['dispatch_agency_id' => 1, 'resource' => 'Police Cruisers','quantity' => '300']);
        Resource::create(['dispatch_agency_id' => 3, 'resource' => 'Singapore Power Maintenance Team','quantity' => '50']);
        
        
        
        /* Crisis Levels table */
        CrisisLevel::create(['crisis_level' => '1']);
        CrisisLevel::create(['crisis_level' => '2']);
        CrisisLevel::create(['crisis_level' => '3']);
        CrisisLevel::create(['crisis_level' => '4']);
        CrisisLevel::create(['crisis_level' => '5']);
        
        /* Crisis table */
        $crisis = CrisisType::create(['crisis_type' => 'Dengue Outbreak']);
        $crisis -> save();
        //$crisis->protocols()->sync([1, 2, 3, 4, 5]);
        
        $crisis = CrisisType::create(['crisis_type' => 'Haze']);
        $crisis -> save();
        //$crisis->protocols()->sync([1, 2, 3, 4, 5]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '1',
            'crisis_level_id' => '1',
            'protocol' => '1. NEA to increase premises check to once a month in affected areas.',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '1',
            'crisis_level_id' => '2',
            'protocol' => '1. NEA to increase premises check to twice a month in affected areas.',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '1',
            'crisis_level_id' => '3',
            'protocol' => '',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '1',
            'crisis_level_id' => '4',
            'protocol' => '',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '1',
            'crisis_level_id' => '5',
            'protocol' => '',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '2',
            'crisis_level_id' => '1',
            'protocol' => '',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '2',
            'crisis_level_id' => '2',
            'protocol' => '',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '2',
            'crisis_level_id' => '3',
            'protocol' => '',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '2',
            'crisis_level_id' => '4',
            'protocol' => '',
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => '2',
            'crisis_level_id' => '5',
            'protocol' => '',
        ]);

        
        /* Regions table */
        Region::create(['region' => 'Central']);
        Region::create(['region' => 'North East']);
        Region::create(['region' => 'North West']);
        Region::create(['region' => 'South East']);
        Region::create(['region' => 'South West']);
        
    }
}
