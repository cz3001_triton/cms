<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Crisis extends Model
{    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crisis';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'crisis_type_id', 'crisis_level_id', 'active'
    ];
    
    /**
     * The crisis type.
     */
    public function crisisType()
    {

        $query = DB::select('SELECT crisis_type FROM crisis_types WHERE id = '.$this->crisis_type_id );
        
        return collect($query);
    }
    
    /**
     * The crisis level.
     */
    public function crisisLevel()
    {

        $query = DB::select('SELECT crisis_level FROM crisis_levels WHERE id = '.$this->crisis_level_id );
        
        return collect($query);
    }
    
    /**
     * The crisis protocol.
     */
    public function protocol()
    {
        $query = DB::select('SELECT protocol FROM crisis_protocols WHERE crisis_type_id = '.$this->crisis_type_id.' AND crisis_level_id = '.$this->crisis_level_id );
        
        return collect($query);
    }
}
