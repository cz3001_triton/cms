<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentDispatchAgency extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'incident_dispatch_agencies';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'incident_id', 'dispatch_agency_id',
    ];
    
    /**
     * The resources the agency uses for the incident.
     */
    public function resources()
    {
        return $this->belongsToMany('App\Resource', 'incident_dispatch_agency_resources')
            ->withPivot('quantity');
    }

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
