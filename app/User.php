<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
     /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles');
    }
    
    
    /**
     * Get user goles
     */
    public function getUserRoles() {
        $list = array();
        $query = DB::table('user_roles')
                    ->distinct()
                    ->join('roles','user_roles.role_id','=','roles.id')
                    ->where('user_id',$this->id)
                    ->get(array('roles.id','roles.role'));
    
        foreach($query as $role) {
            $list[] = $role->role;
        }
        return $list;
    }
    
    /**
     * The roles that belong to the user.
     */
    public function hasRole($roleName)
    {
        foreach ($this->getUserRoles() as $role)
        {
            if ($role == $roleName)
            {
                return true;
            }
        }

        return false;
    }
    
    
    //Not used
    /*public function privileges()
    {
        return $this->hasManyThrough('App\Privilege', 'App\Role');
    }*/
 
    /**
     * The privileges this user has.
     */
    public function getUserPrivileges() {
        $list = array();
        $query = DB::table('user_roles')
                    ->distinct()
                    ->where('user_id',$this->id)
                    ->join('role_privileges','user_roles.role_id','=','role_privileges.role_id')
                    ->join('privileges','role_privileges.privilege_id','=','privileges.id')
                    ->get(array('privileges.id','privileges.privilege'));
    
        foreach($query as $priv) {
            $list[] = $priv->privilege;
        }
        return $list;
    }
    
    /**
     * Checks if user has the privilege that is passed in.
     */
    public function hasPrivilege($privilegeName)
    {
        foreach ($this->getUserPrivileges() as $privilege)
        {
            if ($privilege == $privilegeName)
            {
                return true;
            }
        }

        return false;
    }
}
