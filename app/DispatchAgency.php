<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DispatchAgency extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dispatch_agencies';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agency','number',
    ];
    
    /**
     * The incidents the agency is handling/handled.
     */
    public function incidents()
    {
        return $this->belongsToMany('App\Incident', 'incident_dispatch_agencies');
    }
    
    /**
     * The resources the dispatch agency owns.
     */
    public function resources()
    {
        return $this->hasMany('App\Resource');
    }
    
    /**
     * The default assignments of dispatch agency
     */
    public function default_assignments()
    {
        return $this->belongsToMany('App\IncidentType', 'default_assignments');
    }
    
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
