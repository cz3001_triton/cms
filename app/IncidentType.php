<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'incident_type',
    ];
    
    /**
     * The incidents of this status type.
     */
    public function incidents()
    {
        return $this->hasMany('App\Incident');
    }

    /**
     * The default agencies associated with this status type.
     */
    public function agencies()
    {
        return $this->belongsToMany('App\DispatchAgency', 'default_assignments');
    }


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
