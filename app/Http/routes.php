<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
use Illuminate\Support\Facades\Mail;

Route::group(['middleware' => ['web']], function () {
   
    
    Route::get('/', 'PublicController@index');
    

    // ===============================================
    // INTRANET SECTION =================================
    // ===============================================
    Route::group(array('prefix' => 'intranet'), function()
    {
        Route::get('/', function()
        {
            return view('auth.login');
        })->middleware('guest');
        
        /*Route::get('register', function()
        {
            return view('auth.register');
        });*/
        
        Route::get('/', function()
        {
            if (Auth::user()->hasRole('Administrator')) {
                return redirect()->action('UserController@index');
            }
            if (Auth::user()->hasRole('MHA Decision Maker')) {
                return redirect()->action('MapController@index');
            }
            if (Auth::user()->hasRole('Government Agency')) {
                return redirect()->action('MapController@index');
            }
            if (Auth::user()->hasRole('MHA Public Relations')) {
                return redirect()->action('NotificationController@index');
            }
            if (Auth::user()->hasRole('Resource Controller')) {
                return redirect()->action('DispatchAgencyController@index');
            }
            if (Auth::user()->hasRole('Call Center Operator')) {
                return redirect()->action('IncidentController@create');
            }
            return view('index');
        })->middleware('auth');
        
        // Either (notification or advisory) routes
        //Route::POST('notification/postSocial', 'NotificationController@postSocial');
        Route::resource('notification', 'NotificationController');
        
        // Incident routes
        Route::put('incident/updateAssign', 'IncidentController@updateAssign'); //{dispatch_agency_id}{resource_id}{quantity}
        Route::POST('incident/{incidentID}/close', 'IncidentController@close_incident');
        Route::get('incident/{incidentID}/assign', 'IncidentController@assign');
        Route::resource('incident', 'IncidentController');
        
        // Crisis-type routes
        Route::resource('crisis-type', 'CrisisTypeController');
        
        // Crisis-protocol routes
        Route::get('crisis-protocol/{typeID}/{levelID}', 'CrisisProtocolController@show');
        Route::get('crisis-protocol/{typeID}/{levelID}/edit', 'CrisisProtocolController@edit');
        Route::put('crisis-protocol/{typeID}/{levelID}', 'CrisisProtocolController@update');
        Route::delete('crisis-protocol/{typeID}/{levelID}', 'CrisisProtocolController@destroy');
        Route::get('crisis-protocol/create/{crisisType}', 'CrisisProtocolController@create');
        Route::resource('crisis-protocol', 'CrisisProtocolController');
        
        // Crisis routes
        Route::POST('crisis/{crisisID}/deactivate', 'CrisisController@deactivate');
        Route::get('crisis/incident/{incidentID}', 'IncidentController@show');
        Route::delete('crisis/incident/{incidentID}', 'IncidentController@destroy');
        Route::get('crisis/incident/{incidentID}/edit', 'IncidentController@edit');
        Route::put('crisis/incident/{incidentID}', 'CrisisController@incident_update');
        Route::POST('crisis/incident/{incidentID}/close', 'IncidentController@close_incident');
        Route::resource('crisis', 'CrisisController');
        
        // User routes
        Route::resource('user', 'UserController');
        
        // Role routes
        Route::resource('role', 'RoleController');
        
        // Resource routes
        Route::resource('resource', 'ResourceController');
        
        // Dispatch agency routes
        Route::resource('agency', 'DispatchAgencyController');
        
        // Map routes
        Route::get('/map', 'MapController@index');
        
        // Password reset routes...
        Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
        Route::post('password/reset', 'Auth\PasswordController@postReset');
        
        Route::auth();
		
		
    });
    
    
});
