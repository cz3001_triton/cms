<?php

namespace App\Http\Middleware;

use Closure;

class Privilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $privilege
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->user()->privileges()) {
            // Redirect...
        }

        return $next($request);
    }
}