<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Resource;
use App\DispatchAgency;
use App\Repositories\UserRepository;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Auth;
use Flash;

class ResourceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPrivilege('List Resources')) {
            abort(403);
        }
    $resources =  DB::select('SELECT r.id,
     d.agency,
     r.resource,
     r.quantity,
     coalesce(oc.Count, 0) as Agencies_using,
     resource_used
     from resources r LEFT JOIN dispatch_agencies d ON  r.dispatch_agency_id = d.id
          left join (
         select inc.resource_id, count(*) as Count, SUM(quantity) as resource_used
         from incident_dispatch_agency_resources as  inc
         group by resource_id
     ) oc on (r.id = oc.resource_id) 
    ');
       
       return view('resource.index', ['resources' => $resources]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPrivilege('Create Resources')) {
            abort(403);
        }
       $dispatch_agencies = DispatchAgency::all();
       return view('resource.create', ['dispatchAgencies' => $dispatch_agencies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPrivilege('Create Resources')) {
            abort(403);
        }
        
        $this->validate($request, [
            'resource' => 'required|max:255',
            'quantity' => 'required',
            'dispatch_agency_id' => 'required',
        ]);
        
            $resources = Resource::create([
            'resource' => $request->resource,
            'quantity' => $request->quantity,
            'dispatch_agency_id' => $request->dispatch_agency_id,
        ]);
        
        Flash::success('New resource added!');
            
        return redirect('/intranet/resource');            
                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPrivilege('Edit Resources')) {
            abort(403);
        }       
       $resource =  Resource::findOrFail($id);     
       $dispatch_agencies = DispatchAgency::all();
       return view('resource.edit', ['resource' => $resource,'dispatchAgencies' => $dispatch_agencies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Edit Resources')) {
            abort(403);
        }
        
        $resource = Resource::findOrFail($id);
        
        $this->validate($request, [
            'resource' => 'required|max:255',
            'quantity' => 'required',
            'dispatch_agency_id' => 'required',
        ]);
        
        $resource->resource = $request->resource;
        $resource->quantity = $request->quantity;
        $resource->dispatch_agency_id = $request->dispatch_agency_id;
        $resource->save();
        
        Flash::success('Resource updated!');
        
        return redirect('/intranet/resource');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
