<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Flash;
use SmsGateway;

use DB;
use App\Incident;
use App\IncidentType;
use App\DispatchAgency;
use App\Resource;
use App\Crisis;

class IncidentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function close_incident(Request $request, $id){
       // if (!Auth::user()->hasPrivilege('Update Incidents')) {
         //   abort(403);
        //} 
        $incident = Incident::findOrFail($id);
       
        $incident->statuses()->attach(['incident_status_type_id' => 2], ['user_id' => Auth::user()->id]);    
                                 
        Flash::success('Incident closed!');
        
        $url = parse_url($request->server('HTTP_REFERER'));
        $url = explode('/', $url['path']);
        
        foreach($url as $u) {
            if ($u === 'crisis') {
                return redirect($request->server('HTTP_REFERER'));
            }
        }
        return redirect('/intranet/incident');
        
    }
    
    public function index(Request $request)
    {
        if (!Auth::user()->hasPrivilege('List Incidents')) {
            abort(403);
        }
        
        if (isset($_GET['id'])) {
            $incidents = Incident::with('type','statuses')->where(array('id' => $_GET['id']))->get();
        } else {
            $incidents = Incident::with('type','statuses')->get();
        }
        //if ($request->is('incident/*')) {
            //return view('incident.index', ['incidents' => $incidents]);
        //} else {
            return view('incident.index', ['incidents' => $incidents]);
       // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPrivilege('Create Incidents')) {
            abort(403);
        }
        
        $incidents = Incident::with('type','statuses')->get();
        $incident_types = IncidentType::all();
        $crisis = Crisis::where('active', 1)->get();
        
        return view('incident.create', ['incident_types' => $incident_types, 'incidents' => $incidents, 'crisis' => $crisis]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPrivilege('Create Incidents')) {
            abort(403);
        }
        
        $this->validate($request, [
            'name' => 'required|max:255',
            'mobile_number' => 'required|max:20',
            'postal_code' => 'required',
            'incident_type' => 'required',
        ]);
        
        $incident_type = explode('|', $request->incident_type);
        
        $incident = Incident::create([
            'incident' => $incident_type[1] . ' at ' . $request->address,
            'name' => $request->name,
            'mobile_number' => $request->mobile_number,
            'postal_code' => $request->postal_code,
            'unit_number' => $request -> unit_number,
            'incident_type_id' => $incident_type[0],
            'note' => $request -> note,
        ]);
        if (!empty($request->crisis)) {
            $incident->crisis_id = $request -> crisis;
            $incident->save();
        }
        

        $url = url('intranet/incident?id=');
        $url = $url . $incident->id;
        
        $agencies = IncidentType::with('agencies')->where(array('id' => $incident->incident_type_id))->distinct('dispatch_agency_id')->get();

        include "smsGateway.php";
        foreach ($agencies as $agency) {
            foreach ($agency->agencies as $a) {
                
                $incident->dispatch_agencies()->attach(['dispatch_agency_id' => $a->id]);
                
                $smsGateway = new SmsGateway('tritoncms3003@gmail.com', 'Tritonpassword1');
                $deviceID = 20447;
                $number = $a->number;
                $message = 'Alert: New incident '. $url . ' Proceed to assign resources.';

                $result = $smsGateway->sendMessageToNumber($number, $message, $deviceID);
            }
        }
        $incident->statuses()->attach(['incident_status_type_id' => 1], ['user_id' => Auth::user()->id]);    
        
        Flash::success('New incident added!');
        if (!Auth::user()->hasRole('Call Center Operator')) {
            return redirect('/intranet/incident/create');
        }
        return redirect('/intranet/incident');
 
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->hasPrivilege('List Incidents')) {
            abort(403);
        }
        
        $incident = Incident::with('type')->findOrFail($id);
        
        return view('incident.show', ['incident' => $incident]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Edit Incidents')) {
            abort(403);
        }

        $incident = Incident::with('type')->findOrFail($id);
        $types = IncidentType::all();
        $crisis = Crisis::all();
        
        $url = parse_url($request->server('HTTP_REFERER'));
        $url = explode('/', $url['path']);
        
        foreach($url as $u) {
            if ($u === 'crisis') {
                return view('crisis.incident.edit', ['incident' => $incident, 'type' => $types, 'crisis' => $crisis]);
            }
        }
        return view('incident.edit', ['incident' => $incident, 'type' => $types, 'crisis' => $crisis]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Edit Incidents')) {
            abort(403);
        }
        
        $incident = Incident::findOrFail($id);
         
        $this->validate($request, [
            'incident' => 'required',
            'name' => 'required|max:255',
            'mobile_number' => 'required|max:20',
            'postal_code' => 'required',
            'incident_type' => 'required',
        ]);
        
        $incident->incident = $request->incident;
        $incident->name = $request->name;
        $incident->mobile_number = $request->mobile_number;
        $incident->postal_code = $request->postal_code;
        $incident->unit_number = $request -> unit_number;
        $incident->incident_type_id = $request -> incident_type;
        
        if (!empty($request->crisis)) {
            $incident->crisis_id = $request -> crisis;
        } else {
            $incident->crisis_id = NULL;
        }

        $incident->save();
        

        
        Flash::success('Incident updated!');
        
        return redirect('/intranet/incident');
    }

    
        public function assign(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Assign Resources')) {
            abort(403);
        }

        $incident = Incident::with('type')->findOrFail($id);
        $types = IncidentType::all();
        $resource = Resource::all();
        $dispatch_agencies = DispatchAgency::all();
        
        return view('incident.assign', ['incident' => $incident, 'type' => $types, 'resource' => $resource, 'dispatch_agencies' => $dispatch_agencies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAssign(Request $request)
    {

        if (!Auth::user()->hasPrivilege('Assign Resources')) {
            abort(403);
        }

        //Update Resource Quantity Value
        $resource = Resource::findOrFail($request->resource_id);
        $resource->quantity = $resource->quantity - $request->quantity;
        $resource->save();
        
        $this->validate($request, [
            'incident_id' => 'required',
            'dispatch_agency_id' => 'required',
            'resource_id' => 'required',
            'quantity' => 'required',
        ]);
        //Create Incident - Dispatch Agency 
        DB::table('incident_dispatch_agencies')->insert(
         ['incident_id' => $request->incident_id, 'dispatch_agency_id' => $request->dispatch_agency_id]
        );            
        //Create Dispatch Agency Resources
        DB::table('incident_dispatch_agency_resources')->insert(
         ['incident_id' => $request->incident_id, 'dispatch_agency_id' => $request->dispatch_agency_id, 'resource_id' => $request->resource_id, 'quantity' => $request->quantity]
        );
                
         Flash::success('Resource assigned!');

        return redirect('/intranet/incident');
    }
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Delete Incidents')) {
            abort(403);
        }
                

        try {
            $incident = Incident::findOrFail($id);
            $incident->statuses()->detach();
            $incident->delete();
            
            Flash::success('Incident deleted!');
        } catch(\Illuminate\Database\QueryException $ex){
            Flash::error('Incident cannot be deleted!');
        } 
        
        $url = parse_url($request->server('HTTP_REFERER'));
        $url = explode('/', $url['path']);
        
        foreach($url as $u) {
            if ($u === 'crisis') {
                return redirect($request->server('HTTP_REFERER'));
            }
        }
        return redirect('/intranet/incident');
    }    
}
