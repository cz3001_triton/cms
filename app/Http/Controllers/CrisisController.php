<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Crisis;
use App\CrisisProtocol;
use App\Incident;

use DB;
use Auth;
use Flash;

class CrisisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->authorize('activate', Auth::user());
        
        $crisis = Crisis::all()->sortByDesc('active');
        
        return view('crisis.index', ['crisis' => $crisis]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPrivilege('Activate Crisis')) {
            abort(403);
        }
        
        $crisisProtocols = CrisisProtocol::with('crisis_type','crisis_level')->get()
                ->sortBy(function($crisisProtocols) {
                    return sprintf('%-12s%s', $crisisProtocols->crisis_type_id, $crisisProtocols->crisis_level_id);
                });
        
        return view('crisis.create', ['crisisProtocols' => $crisisProtocols]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPrivilege('Activate Crisis')) {
            abort(403);
        }
        
        $this->validate($request, [
            'crisisProtocol' => 'required',
        ]);
        
        $crisisProtocol = explode('|', $request->crisisProtocol);
        
        $type = DB::table('crisis_types')->where('id', $crisisProtocol[0])->first();
        $name =  $type->crisis_type . ' (' . date('F Y') . ')';
        
        $check = DB::select("SELECT * FROM crisis WHERE crisis_type_id = ". $crisisProtocol[0] ." AND ". " active = 1");
 
        if (empty($check)) {
            Crisis::create([
                'name' => $name,
                'crisis_type_id' => $crisisProtocol[0],
                'crisis_level_id' => $crisisProtocol[1],
                'active' => 1
            ]);
            Flash::success('Crisis activated!');
        } else {
            Flash::error('Crisis already activated!');
        }
        return redirect('/intranet/crisis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $crisis = Crisis::findOrFail($id);
        $incidents = Incident::with('statuses')->where(array('crisis_id' => $id))->get();
        
        return view('crisis.show', ['crisis' => $crisis, 'incidents' => $incidents]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crisis = Crisis::findOrFail($id);
        
        $crisisProtocols = CrisisProtocol::with('crisis_type','crisis_level')->get()
                ->sortBy(function($crisisProtocols) {
                    return sprintf('%-12s%s', $crisisProtocols->crisis_type_id, $crisisProtocols->crisis_level_id);
                });
                
        $crisisProtocols = $crisisProtocols->where('crisis_type_id', $crisis->crisis_type_id);
        
        return view('crisis.edit', ['crisis' => $crisis, 'crisisProtocols' => $crisisProtocols]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $crisis = Crisis::findOrFail($id);
        
        $this->validate($request, [
            'crisisProtocol' => 'required',
        ]);
        
        $crisisProtocol = explode('|', $request->crisisProtocol);

        $crisis->crisis_type_id = $crisisProtocol[0];
        $crisis->crisis_level_id = $crisisProtocol[1];
        $crisis->save();
        
        Flash::success('Crisis protocol updated!');
        
        return redirect('/intranet/crisis/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function deactivate(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Deactivate Crisis')) {
            abort(403);
        }
        
        $crisis = Crisis::findOrFail($id);
       
        $crisis->active = 0;
        $crisis->save();
        
        Flash::success('Crisis deactivated!');

        return redirect('/intranet/crisis');
    }
    
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function incident_update(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Edit Incidents')) {
            abort(403);
        }
        
        $incident = Incident::findOrFail($id);
         
        $this->validate($request, [
            'incident' => 'required',
            'name' => 'required|max:255',
            'mobile_number' => 'required|max:20',
            'postal_code' => 'required',
            'incident_type' => 'required',
        ]);
        
        $incident->incident = $request->incident;
        $incident->name = $request->name;
        $incident->mobile_number = $request->mobile_number;
        $incident->postal_code = $request->postal_code;
        $incident->unit_number = $request -> unit_number;
        $incident->incident_type_id = $request -> incident_type;

        if (!empty($request->crisis)) {
            $incident->crisis_id = $request -> crisis;
        } else {
            $incident->crisis_id = NULL;
        }
        
        $incident->save();
        
        Flash::success('Incident updated!');
        
        return redirect($request->redirect);
    }
}
