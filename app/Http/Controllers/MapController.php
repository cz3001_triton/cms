<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Incident;

class MapController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display the map with ongoing incidents.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Might need to join other tables to get more information about incident status/type/dispatch agencies
        //to show the details in the bubble when user clicks on the marker.
        $incidents = Incident::with('type','statuses')->get();
        
        return view('map.index', ['incidents' => $incidents]);
    }
    
    //Might want to create another controller function for the public map
}
