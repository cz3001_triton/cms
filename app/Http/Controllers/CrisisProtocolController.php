<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CrisisType;
use App\CrisisLevel;
use App\CrisisProtocol;

use DB;
use Auth;
use Flash;

class CrisisProtocolController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPrivilege('List Crisis Protocols')) {
            abort(403);
        }
        
        $crisisProtocols = CrisisProtocol::with('crisis_type','crisis_level')->get()
                ->sortBy(function($crisisProtocols) {
                    return sprintf('%-12s%s', $crisisProtocols->crisis_type_id, $crisisProtocols->crisis_level_id);
                });
        
        return view('crisis-protocol.index', ['crisisProtocols' => $crisisProtocols]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPrivilege('Create Crisis Protocols')) {
            abort(403);
        }
        //$crisisLevelsEnabled = DB::select('SELECT id FROM crisis_levels WHERE id NOT IN (SELECT crisis_level_id FROM crisis_protocols WHERE crisis_type_id = 1)');

        $crisisTypes = CrisisType::all();
        $crisisLevels =  CrisisLevel::all();

        
        return view('crisis-protocol.create', ['crisisTypes' => $crisisTypes, 'crisisLevels' => $crisisLevels]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPrivilege('Create Crisis Protocols')) {
            abort(403);
        }
        
        $this->validate($request, [
                'crisis_type_id' => 'required|unique_with:crisis_protocols,crisis_level_id',
                'crisis_level_id' => 'required'
        ]);
        
        CrisisProtocol::create([
            'crisis_type_id' => $request->crisis_type_id,
            'crisis_level_id' => $request->crisis_level_id,
            'protocol' => $request->protocol,
        ]);
        
        Flash::success('New crisis protocol added!');

        return redirect('/intranet/crisis-protocol');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($typeID, $levelID)
    {
        if (!Auth::user()->hasPrivilege('List Crisis Protocols')) {
            abort(403);
        }
        
        $crisisProtocol = CrisisProtocol::with('crisis_type', 'crisis_level')->where('crisis_type_id', '=', $typeID)
                                        ->where('crisis_level_id', '=', $levelID)
                                        ->first();
        
        return view('crisis-protocol.show', ['crisisProtocol' => $crisisProtocol]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($typeID, $levelID)
    {
        if (!Auth::user()->hasPrivilege('Edit Crisis Protocols')) {
            abort(403);
        }
        
        $crisisProtocol = CrisisProtocol::with('crisis_type', 'crisis_level')->where('crisis_type_id', '=', $typeID)
                                ->where('crisis_level_id', '=', $levelID)
                                ->first();
        
        return view('crisis-protocol.edit', ['crisisProtocol' => $crisisProtocol]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $typeID, $levelID)
    {
        if (!Auth::user()->hasPrivilege('Edit Crisis Protocols')) {
            abort(403);
        }
        
        $this->validate($request, [
            'protocol' => 'required',
        ]);
        
        DB::update('UPDATE crisis_protocols SET protocol = \''.$request->protocol.'\' WHERE crisis_type_id = '.$typeID.' AND crisis_level_id = ' .$levelID);

        Flash::success('Crisis protocol updated!');
        
        return redirect('/intranet/crisis-protocol');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($typeID, $levelID)
    {
        if (!Auth::user()->hasPrivilege('Delete Crisis Protocols')) {
            abort(403);
        }
        
        try {
            DB::delete('DELETE FROM crisis_protocols WHERE crisis_type_id = '.$typeID.' AND crisis_level_id = ' .$levelID);    
            
            Flash::success('Crisis protocol deleted!');
        } catch(\Illuminate\Database\QueryException $ex){
            Flash::error('Crisis protocol cannot be deleted!');
        }
        
        return redirect('/intranet/crisis-protocol');
    }
}