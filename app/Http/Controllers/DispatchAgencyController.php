<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DispatchAgency;
use App\Resource;
use App\IncidentType;

use DB;
use Auth;
use Flash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class DispatchAgencyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPrivilege('List Dispatch Agencies')) {
            abort(403);
        }
        
        $agencies = DispatchAgency::all();
        
        return view('agency.index', ['agencies' => $agencies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPrivilege('Create Dispatch Agencies')) {
            abort(403);
        }
        
        $incidentTypes = IncidentType::all();
        
        return view('agency.create', ['incidentTypes' => $incidentTypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPrivilege('Create Dispatch Agencies')) {
            abort(403);
        }
        
        $this->validate($request, [
            'agency' => 'required|max:255',
            'number' => 'required',
        ]);
        
        $agency = DispatchAgency::create([
            'agency' => $request->agency,
            'number' => $request->number
        ]);
        $request->incidentType = array_filter($request->incidentType);

        if (!empty($request->incidentType)) {
            $defaultAssignments = $request->incidentType;
            $agency->default_assignments()->attach($defaultAssignments);
        }
        Flash::success('New dispatch agency added!');

        return redirect('/intranet/agency');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->hasPrivilege('List Dispatch Agencies')) {
            abort(403);
        }
        
        $agency = DispatchAgency::with('resources', 'default_assignments')->findOrFail($id);
        
        return view('agency.show', ['agency' => $agency]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPrivilege('Edit Dispatch Agencies')) {
            abort(403);
        }
        
        $agency = DispatchAgency::with('resources', 'default_assignments')->findOrFail($id);
        $incidentTypes = IncidentType::all();
        
        return view('agency.edit', ['agency' => $agency, 'incidentTypes' => $incidentTypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Edit Dispatch Agencies')) {
            abort(403);
        }
        
        $agency = DispatchAgency::findOrFail($id);
        
        $this->validate($request, [
            'agency' => 'required|max:255',
            'number' => 'required',
        ]);

        $agency->agency = $request->agency;
        $agency->number = $request->number;
        $agency->save();
        
        $request->incidentType = array_filter($request->incidentType);

        if (!empty($request->incidentType)) {
            $defaultAssignments = $request->incidentType;
            $agency->default_assignments()->sync($defaultAssignments);
        } else {
            $agency->default_assignments()->detach();
        }
        
        Flash::success('Dispatch agency updated!');
        
        return redirect('/intranet/agency');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        if (!Auth::user()->hasPrivilege('Delete Dispatch Agencies')) {
            abort(403);
        }
        
        try {
            $agency = DispatchAgency::findOrFail($id);
            $agency->default_assignments()->detach();
            $agency->delete();
            
            Flash::success('Dispatch agency deleted!');
        } catch(\Illuminate\Database\QueryException $ex){
            Flash::error('Dispatch agency cannot be deleted!');
        }

        return redirect('/intranet/agency');
    }
}
