<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CrisisType;

use Auth;
use Flash;

class CrisisTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPrivilege('List Crisis Types')) {
            abort(403);
        }
        
        $crisisTypes = CrisisType::all();
        
        return view('crisis-type.index', ['crisisTypes' => $crisisTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPrivilege('Create Crisis Types')) {
            abort(403);
        }
        
        return view('crisis-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPrivilege('Create Crisis Types')) {
            abort(403);
        }
        
        $this->validate($request, [
            'crisis_type' => 'required|max:255|unique:crisis_types',
        ]);
        
        CrisisType::create([
            'crisis_type' => $request->crisis_type,
        ]);
        
        Flash::success('New crisis type added!');

        return redirect('/intranet/crisis-type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->hasPrivilege('List Crisis Types')) {
            abort(403);
        }
        
        $crisisType = CrisisType::findOrFail($id);
        
        return view('crisis-type.show', ['crisisType' => $crisisType]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPrivilege('Edit Crisis Types')) {
            abort(403);
        }
        
        $crisisType = CrisisType::findOrFail($id);
        
        return view('crisis-type.edit', ['crisisType' => $crisisType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Edit Crisis Types')) {
            abort(403);
        }
        
        $crisisType = CrisisType::findOrFail($id);
        
        $this->validate($request, [
            'crisis_type' => 'required|max:255|unique:crisis_types',
        ]);

        $crisisType->crisis_type = $request->crisis_type;
        $crisisType->save();
        
        Flash::success('Crisis type updated!');
        
        return redirect('/intranet/crisis-type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->hasPrivilege('Delete Crisis Types')) {
            abort(403);
        }
        
        try {
            $crisisType = CrisisType::findOrFail($id);
            $crisisType->delete();
            
            Flash::success('Crisis type deleted!');
        } catch(\Illuminate\Database\QueryException $ex){
            Flash::error('Crisis type cannot be deleted!');
        }

        return redirect('/intranet/crisis-type');
    }
}
