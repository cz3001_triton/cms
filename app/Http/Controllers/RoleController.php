<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Role;
use App\Privilege;

use DB;
use Auth;
use Flash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPrivilege('List Roles')) {
            abort(403);
        }
        
        $roles = Role::all();
        
        return view('role.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->hasPrivilege('Create Roles')) {
            abort(403);
        }
        
        $privileges = Privilege::all();
        return view('role.create', ['privileges' => $privileges]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->hasPrivilege('Create Roles')) {
            abort(403);
        }
        
        $this->validate($request, [
            'role' => 'required|max:255',
            'privilege' => 'required',
        ]);
        
        $role = Role::create([
            'role' => $request->role,
        ]);

        $privileges = $request->privilege;
        $role->privileges()->attach($privileges);

        Flash::success('New role added!');
        
        return redirect('/intranet/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->hasPrivilege('List Roles')) {
            abort(403);
        }
        
        $role = Role::with('privileges')->findOrFail($id);
        
        return view('role.show', ['role' => $role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->hasPrivilege('Edit Roles')) {
            abort(403);
        }
        
        $role =  Role::with('privileges')->findOrFail($id);
        $privileges = Privilege::all();
        
        return view('role.edit', ['role' => $role, 'privileges' => $privileges]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPrivilege('Edit Roles')) {
            abort(403);
        }
        
        $role = Role::findOrFail($id);
        
        $this->validate($request, [
            'role' => 'required|max:255',
            'privilege' => 'required',
        ]);

        $role->role = $request->role;
        $role->save();
        $privileges = $request->privilege;
        $role->privileges()->sync($privileges);
        
        Flash::success('Role updated!');
        
        return redirect('/intranet/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        if (!Auth::user()->hasPrivilege('Delete Roles')) {
            abort(403);
        }
        
        try {
            $role = Role::findOrFail($id);
            $role->privileges()->detach();
            $role->delete();
            
            Flash::success('Role deleted!');
        } catch(\Illuminate\Database\QueryException $ex){
            Flash::error('Role cannot be deleted!');
        }  

        return redirect('/intranet/role');
    }
}
