<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;

use DB;
use Auth;
use Flash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Auth::user());
        
        $users = User::with('roles')->get();
        
        return view('user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Auth::user());
        
        $roles = Role::all();
        return view('user.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Auth::user());
        
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'role' => 'required',
        ]);
        
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt(str_random(20)),
        ]);

        $roles = $request->role;
        $user->roles()->attach($roles);
        
        $reset_token = strtolower(str_random(64));
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $reset_token,
            'created_at' => Carbon::now(),
        ]);
        
        Mail::send('auth.emails.account_creation',['token' => $reset_token, 'user' => $user], function ($message) use ($user) {
            $message->to($user->email, $user->name)->subject('Your new account on Triton CMS');
        });
        
        Flash::success('New user added!');

        return redirect('/intranet/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Auth::user());
                
        $user = User::with('roles')->findOrFail($id);
        
        return view('user.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Auth::user());
        
        $user =  User::with('roles')->findOrFail($id);
        $roles = Role::all();
        
        return view('user.edit', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Auth::user());
        
        $user = User::findOrFail($id);
        
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$id,
            'role' => 'required',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        $roles = $request->role;
        $user->roles()->sync($roles);
        
        Flash::success('User updated!');
        
        return redirect('/intranet/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('destroy', Auth::user());
        
        try {
            $user = User::findOrFail($id);
            $user->roles()->detach();
            $user->delete();
            
            Flash::success('User deleted!');
        } catch(\Illuminate\Database\QueryException $ex){
            Flash::error('User cannot be deleted!');
        }  
        
        return redirect('/intranet/user');
    }
}
