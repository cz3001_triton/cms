<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

use App\Incident;
use App\IncidentType;
use App\Advisory;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incidents = Incident::with('type','statuses')->get();
        $advisories = Advisory::with('regions')->where('published', 1)->get();
        
        return view('public', ['incidents' => $incidents, 'advisories' => $advisories]);
    }
}
