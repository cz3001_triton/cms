<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Flash;
use DB;

use App\Advisory;
use App\Region;
use App\Http\Requests;

use Codebird\Codebird;
use SmsGateway;

require $_SERVER['DOCUMENT_ROOT'].'/cms/vendor/autoload.php';

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('privilege);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postSocial(Request $request)
    {
            $text = $request->socialpost;
            $cb = new Codebird;
            $cb->setConsumerKey('knfFb1pP207wjlTl8hdSzfm3k', 'ggD32dwL0n4jlzBYbYW98lxmdlOdJjtpEzsDznocGefnxxJKDK');
            $cb->setToken('712474481525071872-Fq1WDih5MdMj4Bi48hB0xX0SgTJFeoJ', 'tlPQRcmV61KLgsHd8HBBccHvq0e7bKvkVcbdAoeA08gMX');
            $cb->statuses_update(['status' => $text]);
           
           echo '<script type= "text/javascript">alert("Post Successful");</script>'; //not working
            return redirect('/intranet/notification');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advisories = Advisory::all();
        $regions = Region::all();
        
        return view('notification.index',['advisories' => $advisories, 'regions' => $regions]); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         if (!Auth::user()->hasPrivilege('Create Advisories')) {
            abort(403);
        }
        $regions = Region::all();     
        return view('notification.create',['regions' => $regions]);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);
        
        $str = 'Message posted to ';
        foreach ($request->post_to as $option) {
            if ($option == 'social') {
                $text = $request->message;
                $cb = new Codebird;
                $cb->setConsumerKey('knfFb1pP207wjlTl8hdSzfm3k', 'ggD32dwL0n4jlzBYbYW98lxmdlOdJjtpEzsDznocGefnxxJKDK');
                $cb->setToken('712474481525071872-Fq1WDih5MdMj4Bi48hB0xX0SgTJFeoJ', 'tlPQRcmV61KLgsHd8HBBccHvq0e7bKvkVcbdAoeA08gMX');
                $cb->statuses_update(['status' => $text]);
                $str = $str . 'Facebook & Twitter';
            }
            if ($option == 'advisory') {
                $this->validate($request, [
                    'region' => 'required',
                ]);
                
                $advisory = Advisory::create([
                    'message' => $request->message,
                    'user_id' => Auth::user()->id,
                    'published' => 1
                ]);
                
                $regions = $request->region;
                $advisory->regions()->attach($regions);
                $str = $str . 'Advisories';
            }
            if ($option == 'sms') {
                include "smsGateway.php";
                $smsGateway = new SmsGateway('tritoncms3003@gmail.com', 'Tritonpassword1');
                $list = array();

                $persons = DB::connection('external')->select('SELECT number FROM persons');
                foreach ($persons as $person) {
                    $list[] = $person->number;
                }
                
                $deviceID = 20447;
                $number = $list;
                $message = $request->message;

                $result = $smsGateway->sendMessageToNumber($number, $message, $deviceID);
                
                $str = $str . 'SMS';

            }
        }
        
        Flash::success($str);
        
        return redirect('/intranet/notification');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (!Auth::user()->hasPrivilege('List Advisories')) {
            abort(403);
        }

        $advisory = Advisory::with('regions')->findOrFail($id);
        
        return view('notification.show', ['advisory' => $advisory]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (!Auth::user()->hasPrivilege('Edit Roles')) {
            abort(403);
        }
        $regions = Region::all(); 
        $advisory = Advisory::with('regions')->findOrFail($id);
        
        return view('notification.edit', ['advisory' => $advisory, 'regions' =>$regions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         if (!Auth::user()->hasPrivilege('Edit Advisories')) {
            abort(403);
        }
        
        $advisory = Advisory::findOrFail($id);
         
        $this->validate($request, [
            'message' => 'required'
        ]);
            
        $advisory->message = $request->message;
        if ($advisory->published == 'published') {
            $advisory->published = 1;
        } else {
            $advisory->published = 0;
        }
        $advisory->save();
        
        $region = $request->regions;
        $advisory->regions()->sync($region);
        
        Flash::success('Advisory updated!');
        
        return redirect('/intranet/notification');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (!Auth::user()->hasPrivilege('Delete Advisories')) {
            abort(403);
        }
                
        $advisory = Advisory::findOrFail($id);
        $advisory->regions()->detach();
        $advisory->delete();
        
        Flash::success('Advisory deleted!');

        return redirect('/intranet/notification');
    }
}
