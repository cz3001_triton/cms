<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Validator\CompositeValidator;

use App\Crisis;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $crisis_nav = Crisis::all()->where('active', 1);
        
        view()->share('crisis_nav', $crisis_nav);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
