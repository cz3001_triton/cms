<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Role' => 'App\Policies\RolePolicy',
        'App\Crisis' => 'App\Policies\CrisisPolicy',
        'App\CrisisType' => 'App\Policies\CrisisTypePolicy',
        'App\CrisisProtocol' => 'App\Policies\CrisisProtocolPolicy',
        'App\Resource' => 'App\Policies\ResourcePolicy',
        'App\Incident' => 'App\Policies\IncidentPolicy',
        'App\Advisory' => 'App\Policies\AdvisoryPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
