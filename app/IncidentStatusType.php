<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentStatusType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'incident_status_type',
    ];
    
    /**
     * The incidents of this status type.
     */
    public function incidents()
    {
        return $this->belongsToMany('App\Incident', 'incident_status');
    }

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
