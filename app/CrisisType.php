<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrisisType extends Model
{    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'crisis_type',
    ];
        
    /**
     * 
     */
    public function protocols()
    {
        return $this->belongsToMany('App\CrisisLevel', 'crisis_protocols')
                ->withPivot('protocol')
                ->withTimestamps();
    }
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
