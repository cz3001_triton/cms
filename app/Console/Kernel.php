<?php

namespace App\Console;

use DB;
use Illuminate\Support\Facades\Mail;
use App\Crisis;
use App\Incident;
use App\DispatchAgency;
use App\Resource;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function () {
            
            $crisis = Crisis::all()->where('active', 1);
            
            $list = array();
            foreach ($crisis as $c) {
                
                $list[] = $c->id;
            }
            $incidents = Incident::with('statuses', 'dispatch_agencies')->where(array('crisis_id' => $list))->get();
            
            $resources =  DB::select('SELECT r.id,
     d.agency,
     r.resource,
     r.quantity,
     coalesce(oc.Count, 0) as Agencies_using,
     resource_used
     from resources r LEFT JOIN dispatch_agencies d ON  r.dispatch_agency_id = d.id
          left join (
         select inc.resource_id, count(*) as Count, SUM(quantity) as resource_used
         from incident_dispatch_agency_resources as  inc
         group by resource_id
     ) oc on (r.id = oc.resource_id)  
                               ');
            
            $agencies = DB::select('SELECT agency FROM dispatch_agencies WHERE id IN (SELECT dispatch_agency_id FROM incident_dispatch_agencies ida, incidents i WHERE ida.incident_id = i.id AND i.crisis_id IN (SELECT id FROM crisis WHERE active = 1))');
            
            if (!empty($crisis)) {
                Mail::send('notification.emails.pmo_email',['crisis' => $crisis, 'incidents' => $incidents, 'agencies' => $agencies, 'resources' => $resources], function ($message) {
                    $message->to('tritoncms3003@gmail.com', 'Prime Minister')->subject('Update On Crisis');
                });
            }
        })->everyMinute();
    }
}
