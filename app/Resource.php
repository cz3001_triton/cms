<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Resource extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'resource', 'quantity','dispatch_agency_id'
    ];
    
    /**
     * The incidents the agency is handling/handled.
     */
    public function incidents()
    {
        return $this->belongsToMany('App\IncidentDispatchAgency', 'incident_dispatch_agency_resources');
    }
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
