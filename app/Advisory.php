<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advisory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'advisories';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'message', 'published',
    ];
        
    /**
     * The regions that the advisory is targeted at.
     */
    public function regions()
    {
        return $this->belongsToMany('App\Region', 'advisory_regions', 'advisory_id', 'region_id');
    }
    public function user_created()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
