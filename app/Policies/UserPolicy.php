<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine if the given user can create user.
     *
     * @param  User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPrivilege('Create Users');
    }
    
    /**
     * Determine if the given user can view users.
     *
     * @param  User  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->hasPrivilege('List Users');
    }
    
    /**
     * Determine if the given user can update the given user.
     *
     * @param  User  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasPrivilege('Edit Users');
    }
    
    /**
     * Determine if the given user can delete the given user.
     *
     * @param  User  $user
     * @return bool
     */
    public function destroy(User $user)
    {
        return $user->hasPrivilege('Delete Users');
    }
}
