<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class IncidentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine if the given user can create incident.
     *
     * @param  User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPrivilege('Create Incidents');
        
    }
    
    /**
     * Determine if the given user can view incidents.
     *
     * @param  User  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->hasPrivilege('List Incidents');
    }
    
    /**
     * Determine if the given user can update the given incident.
     *
     * @param  User  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasPrivilege('Edit Incidents');
    }
    
    /**
     * Determine if the given user can delete the given incident.
     *
     * @param  User  $user
     * @return bool
     */
    public function destroy(User $user)
    {
        return $user->hasPrivilege('Delete Incidents');
    }
}
