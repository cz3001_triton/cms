<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class CrisisPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given user can activate crisis.
     *
     * @param  User  $user
     * @return bool
     */
    public function activate(User $user)
    {
        return $user->hasPrivilege('Activate Crisis');
    }
    
    
    /**
     * Determine if the given user can update the given user.
     *
     * @param  User  $user
     * @return bool
     */
    public function deactivate(User $user)
    {
        return $user->hasPrivilege('Deactivate Crisis');
    }
}
