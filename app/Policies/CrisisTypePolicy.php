<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class CrisisTypePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine if the given user can create crisis type.
     *
     * @param  User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPrivilege('Create Crisis Types');
    }
    
    /**
     * Determine if the given user can view crisis types.
     *
     * @param  User  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->hasPrivilege('List Crisis Types');
    }
    
    /**
     * Determine if the given user can update the given crisis type.
     *
     * @param  User  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasPrivilege('Edit Crisis Types');
    }
    
    /**
     * Determine if the given user can delete the given crisis type.
     *
     * @param  User  $user
     * @return bool
     */
    public function destroy(User $user)
    {
        return $user->hasPrivilege('Delete Crisis Types');
    }
}
