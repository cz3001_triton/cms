<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine if the given user can create role.
     *
     * @param  User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPrivilege('Create Roles');
    }
    
    /**
     * Determine if the given user can view roles.
     *
     * @param  User  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->hasPrivilege('List Roles');
    }
    
    /**
     * Determine if the given user can update the given role.
     *
     * @param  User  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasPrivilege('Edit Roles');
    }
    
    /**
     * Determine if the given user can delete the given role.
     *
     * @param  User  $user
     * @return bool
     */
    public function destroy(User $user)
    {
        return $user->hasPrivilege('Delete Roles');
    }
}
