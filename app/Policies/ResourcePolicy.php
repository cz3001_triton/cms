<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class ResourcePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * Determine if the given user can create resource.
     *
     * @param  User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPrivilege('Create Resources');
    }
    
    /**
     * Determine if the given user can view resources.
     *
     * @param  User  $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->hasPrivilege('List Resources');
    }
    
    /**
     * Determine if the given user can update the given resource.
     *
     * @param  User  $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->hasPrivilege('Edit Resources');
    }
    
    /**
     * Determine if the given user can delete the given resource.
     *
     * @param  User  $user
     * @return bool
     */
    public function destroy(User $user)
    {
        return $user->hasPrivilege('Delete Resources');
    }
}
