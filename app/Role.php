<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
    ];
    
    /**
     * The roles that belong to the user.
     */
    public function privileges()
    {
        return $this->belongsToMany('App\Privilege', 'role_privileges');
    }
    
    public $timestamps = false;
}
