<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrisisProtocol extends Model
{    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'crisis_type_id', 'crisis_level_id', 'protocol'
    ];
    
     /**
     * The type of crisis.
     */
    public function crisis_type()
    {
        return $this->belongsTo('App\CrisisType', 'crisis_type_id', 'id');
    }
    
    public function crisis_level()
    {
        return $this->belongsTo('App\CrisisLevel', 'crisis_level_id', 'id');
    }
}
