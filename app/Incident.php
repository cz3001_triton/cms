<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'incident', 'name', 'mobile_number', 'postal_code', 'unit_number', 'incident_type_id', 'crisis_id', 'note'
    ];
    
    /**
     * The type of incident.
     */
    public function type()
    {
        return $this->belongsTo('App\IncidentType', 'incident_type_id', 'id');
    }
    
    /**
     * The statuses for the incident.
     */
    public function statuses()
    {
        return $this->belongsToMany('App\IncidentStatusType', 'incident_status')
            ->withPivot('user_id')
            ->withTimestamps(); /*below only if this line not working*/
        /*return $this->belongsToMany('App\Incident_Status_Type', 'incident_status', 'incident_id', 'incident_status_type_id');*/
    }
    
    public function status()
    {        
        return $this->statuses()->latest()->limit(1);
            
    }
    /**
     * The agencies dispatched for the incident.
     */
    public function dispatch_agencies()
    {
        return $this->belongsToMany('App\DispatchAgency', 'incident_dispatch_agencies', 'incident_id', 'dispatch_agency_id');
    }
    
    /**
     * The agencies dispatched for the incident.
     */
    public function crisis()
    {
        return $this->belongsTo('App\Crisis', 'crisis_id');
    }
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
