<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
    
class AuthenticationTest extends TestCase
{  
  
    /** @test */
public function testLogin()
{
        
    $this->visit('cms/public/intranet/login')
            ->type('admin@tritoncms.gov.sg', 'email')
            ->type('password','password')
            ->press('Login')       
            ->SeePageIs('/intranet/users');
    
    return Session::all();
}

/** @depends testLogin **/
    public function createIncidentTest($session)
    {
                foreach($session as $key => $value){
                    Session::set($key, $value);
                }
        
                $this->visit('cms/public/intranet/incident/create')
                ->type('test', 'name')
                ->type('12345','mobile_number')
                ->type('632154','postal_code')
                ->type('97512','unit_number')
                ->select('Emergency Ambulance','incident_type')
                ->type('notes here','note')
                ->press('Submit Incident')
                ->seePageIs('/incident');
    }


    }

